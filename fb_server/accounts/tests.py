import json

from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


class FollowTestCase(TestCase):
    def setUp(self):
        self.tester1 = get_user_model().objects.create_user(username='tester1', password='pwd12345678')
        self.tester2 = get_user_model().objects.create_user(username='tester2', password='pwd12345678')
        self.tester3 = get_user_model().objects.create_user(username='tester3', password='pwd12345678')

    def test_user_have_no_followee_when_user_is_just_registered(self):
        """막 가입한 사용자는 팔로우하고 있는 사용자가 없다"""
        token = Token.objects.get(user=self.tester3)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.get('/list/3/', format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(content['followees'], [])

    def test_user_can_follow_someone(self):
        """사용자는 다른 사용자를 팔로우 할 수 있다"""
        token = Token.objects.get(user=self.tester1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/list/1/', {
            'add_followees': True,
            'followees': [2]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(len(content['followees']), 1)
        self.assertEqual(content['followees'], [2])

    def test_users_can_follow_each_others(self):
        """사용자들끼리 서로 팔로우 할 수 있다"""
        token = Token.objects.get(user=self.tester1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/list/1/', {
            'add_followees': True,
            'followees': [2]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(len(content['followees']), 1)
        self.assertEqual(content['followees'], [2])

        token = Token.objects.get(user=self.tester2)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/list/2/', {
            'add_followees': True,
            'followees': [1]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(len(content['followees']), 1)
        self.assertEqual(content['followees'], [1])

    def test_user_can_follow_someones(self):
        """사용자는 다른 여러 사용자를 팔로우 할 수 있다"""
        token = Token.objects.get(user=self.tester1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/list/1/', {
            'add_followees': True,
            'followees': [2]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        response = client.patch('/list/1/', {
            'add_followees': True,
            'followees': [3]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(content['followees'], [2, 3])

    def test_user_can_unfollow_someone(self):
        """사용자는 다른 사용자를 언팔로우 할 수 있다"""
        token = Token.objects.get(user=self.tester1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/list/1/', {
            'add_followees': True,
            'followees': [2]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        response = client.patch('/list/1/', {
            'add_followees': True,
            'followees': [3]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        response = client.patch('/list/1/', {
            'add_followees': False,
            'followees': [2]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(content['followees'], [3])

    def test_user_can_unfollow_someone_when_he_or_she_is_not_following(self):
        """사용자는 팔로우 하지 않는 사용자를 언팔로우 할 수 있다"""
        token = Token.objects.get(user=self.tester1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/list/1/', {
            'add_followees': False,
            'followees': [2]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(content['followees'], [])

    def test_user_can_duplicated_follow(self):
        """사용자는 중복으로 팔로우를 할 수 있다"""
        token = Token.objects.get(user=self.tester1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/list/1/', {
            'add_followees': True,
            'followees': [2]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(content['followees'], [2])
        response = client.patch('/list/1/', {
            'add_followees': True,
            'followees': [2]
        }, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(content['followees'], [2])

    def test_user_can_register_profile_image(self):
        """사용자는 이미지를 등록할 수 있다"""
        token = Token.objects.get(user=self.tester1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/list/1/', {
            'last_name': '테스트'
        }, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(content['last_name'], '테스트')
