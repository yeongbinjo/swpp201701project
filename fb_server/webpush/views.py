from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from webpush.models import Subscription
from webpush.permissions import IsOwner
from webpush.serializers import SubscriptionSerializer


class SubscriptionViewSet(viewsets.ModelViewSet):
    queryset = Subscription.objects.all()
    serializer_class = SubscriptionSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def perform_create(self, serializer):
        if not Subscription.objects.filter(**serializer.validated_data, user=self.request.user).exists():
            serializer.save(user=self.request.user)
