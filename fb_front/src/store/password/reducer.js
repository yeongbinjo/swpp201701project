
import { initialstate } from './selectors'

const verificationReducer = (state = initialstate, action) => {
  switch (action.type) {
    case 'SUCCESS':
      return{
        ...state,
        verified: true,
      }
    case 'RESET':
      return(
        initialstate
      )
    default:
      return state;
  }
}

export default verificationReducer
