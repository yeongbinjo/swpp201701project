import { initialstate } from './selectors'

const questReducer = (state = initialstate, action) => {
  switch (action.type) {
    case 'QUEST_LIST_GET':
      let questListParser = action.questList
      for(let x of questListParser){
        x.regions = JSON.parse(x.regions)
      }
      return {
        ...state,
        questList: questListParser,
        questGetState:1,
      }
      case 'QUEST_RELOAD':
        return initialstate
      default:
        return state
    }
}

export default questReducer
