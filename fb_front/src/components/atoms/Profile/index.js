import React from 'react'
import PropTypes from 'prop-types'
import { Card, CardHeader, CardMedia, CardTitle } from 'material-ui/Card'
import FlatButton from 'material-ui/FlatButton'
import { backendUrl } from 'config'
import { connect } from 'react-redux'


const styles = {
  image: {
    maxHeight: '500px',
    width: 'auto',
    borderBottomRightRadius: '5px',
    borderBottomLeftRadius: '5px',
  },
}

class Profile extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      expanded: false,
    }
  }

  handleExpandChange = (expanded) => {
    this.setState({ expanded })
  };

  handleToggle = (event, toggle) => {
    this.setState({ expanded: toggle })
  };

  handleExpand = () => {
    this.setState({ expanded: true })
  };

  handleReduce = () => {
    this.setState({ expanded: false })
  };

  render() {
    const fullname = `${this.props.user.last_name}${this.props.user.first_name}`
    let avatarOrDefault = this.props.user.profile_image ? this.props.user.profile_image : '/img/avatar.png'
    if (avatarOrDefault.substring(0, 2) === '/m') avatarOrDefault = backendUrl + avatarOrDefault
    let titleOverlay = "아직 완료한 퀘스트가 없네요 T_T "
    if(this.props.user.quest_done!=[])titleOverlay="기본 스킨 착용 중!"
    for(let x of this.props.questState.questList){
      if(x.id == this.props.user.theme_number){
        titleOverlay = "달성한 퀘스트 : "+ x.name
        break
      }
    }
    //if(this.state.expanded) avatar = null
    return (
      <Card zDepth={0} style={{ borderRadius: '5px', border: '1px solid rgba(0,0,0,0.1)'}} expanded={this.state.expanded} onExpandChange={this.handleExpandChange}>
        <CardHeader
          title={fullname}
          subtitle={this.props.user.username}
          actAsExpander
          showExpandableButton
        />
        <CardMedia
          expandable={true}
          overlay={<CardTitle title={fullname} subtitle={titleOverlay} />}
        >
          <img src={avatarOrDefault} style={styles.image} alt="" />
        </CardMedia>
      </Card>
    )
  }
}
Profile.propTypes = {
  user: PropTypes.object,
}

const mapStateToProps = (state) => {
  return {
    questState:state.quest
  }
}

export default connect(mapStateToProps)(Profile)
