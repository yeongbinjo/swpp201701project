import React from 'react'
import PropTypes from 'prop-types'
import WorldMap from '../../atoms/WorldMap'
import { connect } from 'react-redux'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import ActionAndroid from 'material-ui/svg-icons/action/android';
import { questListRequest, userQuestUpdate, questDoneRequest } from '../../../store/quest/actions'
import Progress from 'react-progressbar';


const styles = {
  button: {
    margin: 12,
  },
  card :{
    paddingBottom :'150px',
  }
}
const QuestCard=({quest, onPick})=>{
  const handleQuestPick = ()=>{
    onPick(quest.id)
  }
  return(
    <Card style={styles.card}>
      <CardMedia
        overlay={<CardTitle title={quest.name} />}
      >
      <img src={quest.image} alt="" />
      </CardMedia>
      <CardText>
      {quest.explanation}
      <p>방문해야 하는 국가</p>
      {quest.regions.map((elements, index)=>
        <p key ={index}>{elements}</p>
      )}
      </CardText>
      <CardActions>
        <RaisedButton label="이 퀘스트 고르기!" onTouchTap={handleQuestPick}/>
      </CardActions>
    </Card>
  )
}


const Quest=({wallState, loginState, questState, onLoadQuest, onQuestPick, onQuestDone})=> {
  if(loginState.user.quest_on>0 && questState.questGetState){
    let quest
    for(let x of questState.questList){
      if(x.id==loginState.user.quest_on){
        quest=x
        break;
      }
    }
    let hit = 0;
    let all = 0;
    for(let region of quest.regions){
      if(loginState.user.visited.includes(region)){
        hit++;
      }
      all++;
    }
    const handleQuestDone = () =>{
      onQuestDone(loginState.user.quest_on)
    }
    const FinishButton = ()=>{
      if(hit == all){
        return(
          <RaisedButton
            label="완료!"
            labelPosition="before"
            primary={true}
            style={styles.button}
            onTouchTap={handleQuestDone}
          />
        )
      }
      else return(
        <div></div>
      )
    }
    return(
      <Card>
        <CardHeader
          title="내가 선택한 퀘스트"
        />
        <CardMedia
          overlay={<CardTitle title={quest.name} />}
        >
        <img src={quest.image} alt="" />
        </CardMedia>
        <CardText>
        {quest.explanation}
        </CardText>
        <Progress completed={(hit/all)*100} />
        <CardText>
        {"진척도: "+((hit/all)*100)+"%"}
        </CardText>
        <FinishButton/>
      </Card>
    )
  }
  else {
    let questShow=[]

    for(let x of questState.questList){
      if(loginState.user.quest_done.includes(x.id))continue
      else questShow.push(x)
    }
    return(
      <div>
      <h1>퀘스트를 선택해 보세요!</h1>
      {questShow.map((elements, index)=>
        <QuestCard key={index} quest={elements} onPick={onQuestPick}/>
        )
      }
      </div>
    )
  }
}

Quest.propTypes = {
  params: PropTypes.object,
}

const mapStateToProps = (state) => {
  return {
    wallState: state.wall,
    loginState: state.login,
    questState: state.quest,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onQuestPick:(questID)=>{
      dispatch(userQuestUpdate(questID))
    },
    onQuestDone:(questID)=>{
      dispatch(questDoneRequest(questID))
    },
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Quest)
