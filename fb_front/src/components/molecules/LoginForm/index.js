import React, { PropTypes } from 'react'
import Button from '../../atoms/Button'
import { initialstate } from '../../../store/login/selectors'
import { Link, browserHistory, push } from 'react-router'
import { connect } from 'react-redux'
import TextField from 'material-ui/TextField';

import { pushLogin, updateLogin, pushLogout } from '../../../store/login/actions'

const styles = {
  button: {
    marginLeft:"30px",
    marginRight:"20px"
  },
}

class LoginForm extends React.Component{

constructor() {
  super()
    this.handleId = this.handleId.bind(this)
    this.handlePw = this.handlePw.bind(this)

    this.state = {
      id:"",
      pw:""
    }
  }

handleId(event, value) {
    this.setState({
      id: value,
    })
  }

handlePw(event, value) {
    this.setState({
      pw: value,
    })
  }

  render(){
    let id
    let pw
    const onSubmit=()=>{
      this.props.onPushLogin(this.state.id,this.state.pw)
    }
    const onEnter=(e)=>{
      if(e.keyCode==13){
        this.props.onPushLogin(this.state.id,this.state.pw)
      }
    }


    if(this.props.statefunction.isLoggedOn){
      return(
        <div>
          <h1>hello! {this.props.statefunction.user.first_name}</h1>
          <Button id="logout" type="submit" onClick={this.props.onPushLogout}>Logout</Button>
        </div>
      )
    }
    else{
      return (
        <div>
        <h4>로그인 정보를 입력해주세요</h4>
            <h4><TextField id="ID" onChange={this.handleId}
      hintText="ID"
    /></h4>
            <h4><TextField id="password" type="password" onKeyDown={onEnter}
            onChange={this.handlePw}
      hintText="password"
    /></h4>

        <Button id="login" type="submit" onClick={onSubmit} style={styles.button}>Login</Button>
        <Button id="signup" type="submit"><Link to="/signUp" style={{ textDecoration: 'none' ,color:'white'}}>SignUp</Link></Button>
        </div>
      )
    }
  }
}


const mapStateToProps = (state) =>{
  return {
    statefunction : state.login
    }
}
const mapDispatchToProps= (dispatch)=>{
  return{
    onPushLogin : (id, pw)=> {
      dispatch(pushLogin(id, pw))
    },
    onMount:(token, account)=>{
      dispatch(updateLogin(token,account))
    },
    onPushLogout: ()=>{
      dispatch(pushLogout())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)
