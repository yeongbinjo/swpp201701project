import { take, put, call, fork, select } from 'redux-saga/effects'
import api from 'services/api'
import { backendUrl } from 'config'
import { getID, getToken, getQuestDone } from '../login/selectors'



export function* questListRequest(){
  const response = yield call(api.get,`${backendUrl}/quests/`)
  yield put({type:"QUEST_LIST_GET",questList:response})
}
export function* questListFlow() {
    while (true) {
    yield take('QUEST_LIST_REQUEST')
    yield call(questListRequest)
    }
}

export function* userQuestUpdate(questID){
  const userid = yield select(getID)
  const token = yield select(getToken)
  const response = yield call(api.patch, `${backendUrl}/list/${userid}/`, {quest_on:questID
  },{
     method: 'PATCH',
     mode:'cors',
     headers: {
       Accept: 'application/json, application/xml, text/plain, text/html, *.*',
       'Content-Type': 'application/json',
       Authorization: `Token ${token}`,
     },
   })
   yield put({type:'UPDATE_LOGIN',token:token, user:response})
}

export function* userQuestUpdateFlow() {
    while (true) {
    const {questID} = yield take('USER_QUEST_UPDATE')
    yield call(userQuestUpdate, questID)
    }
}


export function* questDoneUpdate(questID){
  const userid = yield select(getID)
  const token = yield select(getToken)
  const quest_done = yield select(getQuestDone)
  quest_done.push(questID)
  const response = yield call(api.patch, `${backendUrl}/list/${userid}/`, {quest_done:quest_done
  },{
     method: 'PATCH',
     mode:'cors',
     headers: {
       Accept: 'application/json, application/xml, text/plain, text/html, *.*',
       'Content-Type': 'application/json',
       Authorization: `Token ${token}`,
     },
   })
   if(response){
     const response2 = yield call(api.patch, `${backendUrl}/list/${userid}/`, {quest_on:null
     },{
        method: 'PATCH',
        mode:'cors',
        headers: {
          Accept: 'application/json, application/xml, text/plain, text/html, *.*',
          'Content-Type': 'application/json',
          Authorization: `Token ${token}`,
        },
      })
      yield put({type:'UPDATE_LOGIN',token:token, user:response2})
   }
}

export function* questDoneFlow(){
  while(true){
    const {questID} = yield take('QUEST_DONE_REQUEST')
    yield call (questDoneUpdate, questID)
  }
}

export default function* () {
  yield fork(questListFlow)
  yield fork(userQuestUpdateFlow)
  yield fork(questDoneFlow)
}
