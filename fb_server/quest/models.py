from django.db import models
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import AbstractUser


# triggered when new user is created,
def upload_to_image(instance, filename):
    return 'quest_images/{}/image_{}'.format(instance.name, filename)


def upload_to_reward(instance, filename):
    return 'quest_images/{}/reward_{}'.format(instance.name, filename)


class Quest(models.Model):
    name = models.TextField()
    regions = models.TextField(default="[]")
    explanation = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    image = models.ImageField(null=True, blank=True, upload_to=upload_to_image)
    reward = models.ImageField(null=True, blank=True, upload_to=upload_to_reward)

