import newsFeedReducer from './reducer'
import { initialstate } from './selectors'
import totalinitialstate from '../totalinitialstate'

describe('newsFeed Reducer', () => {
  it('has a default state', () => {
    expect(newsFeedReducer(undefined, { type: 'unexpected' })).toEqual(
      initialstate
    )
  })

  it('set NewsFeed', () => {
    expect(newsFeedReducer(undefined, { type: 'SET_NEWSFEEDS', newsFeeds: ['post1', 'post2'] })).toEqual({
      ...initialstate,
      newsFeeds: ['post1', 'post2'],
    })
  })
})
