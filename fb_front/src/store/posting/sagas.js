import { call, put, fork, take, select } from 'redux-saga/effects'
import { backendUrl } from 'config'
import { getImage, getPostState } from './selectors'
import { getID, getToken } from '../login/selectors'
import api from 'services/api'

const url = backendUrl

export function* posting(text, ownerID, tags) {
  try {
    const token = yield select(getToken)
    const id = yield select(getID)
    const image = yield select(getImage)
    const formData = new FormData()
    formData.append('owner', ownerID)
    formData.append('content', text)
    formData.append('tags', tags)
    if (image) formData.append('image', image)
    const response = yield call(fetch, `${url}/posts/`, {
      method: 'POST',
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        Authorization: `Token ${token}`,
      },
      body: formData,
    })
    if (response.status === 201) {
      yield put({ type: 'POST_SUCCESS' })
      yield put({ type: 'SUCCESS_CHECKER' })
      yield put({ type: 'WALL_PARTIAL_UPDATE', updateType: 0, id: ownerID })
      yield put({type:'NEWSFEED_RESET'})
      const user = yield call(api.get, `${url}/list/${id}/`)
      const user2 = yield call(api.get, `${url}/list/${user.id}/`)
      user.profile_image = user2.profile_image
      yield put({ type: 'UPDATE_LOGIN', token: token.token, user })
      yield put({type:'QUEST_RELOAD'})

    }
  } catch (e) {
    console.log(e)
    alert('error')
  }
}

export function* postFlow() {
  while (true) {
    const { text, ownerID, tags } = yield take('PUSH_POST')
    yield call(posting, text, ownerID, tags)
  }
}

export function* stateChecker() {
  const postState = yield select(getPostState)
  if (postState === 2) yield put({ type: 'AFTER_SUCCESS' })
  else yield put({ type: 'SUCCESS_CHECKER' })
}

export function* stateCheckFlow() {
  while (true) {
    yield take('SUCCESS_CHECKER')
    yield call(stateChecker)
  }
}

export default function* () {
  yield fork(postFlow)
  yield fork(stateCheckFlow)
}
