import React from 'react'
import PropTypes from 'prop-types'
import Avatar from 'material-ui/Avatar'
import { List, ListItem } from 'material-ui/List'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import Subheader from 'material-ui/Subheader'
import IconButton from 'material-ui/IconButton'
import { grey400 } from 'material-ui/styles/colors'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import Divider from 'material-ui/Divider'
import { backendUrl } from 'config'

const iconButtonElement = (
  <IconButton touch>
    <MoreVertIcon color={grey400} />
  </IconButton>
)

export class UserList extends React.Component {
  componentDidMount() {
    this.props.onMount()
  }

  render() {
    const followMenu = (userId, followeeId) => (
      <IconMenu iconButtonElement={iconButtonElement}>
        <MenuItem onTouchTap={() => this.props.follow(userId, followeeId)}>팔로우</MenuItem>
      </IconMenu>
    )

    const unfollowMenu = (userId, followeeId) => (
      <IconMenu iconButtonElement={iconButtonElement}>
        <MenuItem onTouchTap={() => this.props.unfollow(userId, followeeId)}>언팔로우</MenuItem>
      </IconMenu>
    )

    const followees = this.props.userList.filter((user) => {
      return this.props.my.followees.includes(user.id) && this.props.my.id !== user.id
    })
    const users = this.props.userList.filter((user) => {
      return !this.props.my.followees.includes(user.id) && this.props.my.id !== user.id
    })

    const getProfileImageUrl = (user) => {
      let avatarOrDefault = user.profile_image ? user.profile_image : '/img/avatar.png'
      if (avatarOrDefault.substring(0, 2) === '/m') avatarOrDefault = backendUrl + avatarOrDefault
      return avatarOrDefault
    }

    return (
      <List>
        <Subheader>팔로우하고 있는 사람들</Subheader>
        <Divider />
        {followees.map(user =>
          <div key={user.id}>
            <ListItem
              primaryText={user.last_name + user.first_name}
              leftAvatar={<Avatar src={getProfileImageUrl(user)} />}
              rightIconButton={unfollowMenu(this.props.my.id, user.id)}
              onTouchTap={() => this.props.onUserTap(user)}
            />
            <Divider />
          </div>
        )}
        <Subheader>알 수도 있는 사람들</Subheader>
        <Divider />
        {users.map(user =>
          <div key={user.id}>
            <ListItem
              primaryText={user.last_name + user.first_name}
              leftAvatar={<Avatar src={getProfileImageUrl(user)} />}
              rightIconButton={followMenu(this.props.my.id, user.id)}
              onTouchTap={() => this.props.onUserTap(user)}
            />
            <Divider />
          </div>
        )}
      </List>
    )
  }
}

UserList.propTypes = {
  my: PropTypes.object,
  userList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    first_name: PropTypes.string,
    last_name: PropTypes.string,
  })),
  onUserTap: PropTypes.func,
  onMount: PropTypes.func,
  follow: PropTypes.func,
  unfollow: PropTypes.func,
}
