'use strict';

self.addEventListener('push', function(event) {
  console.log('[Service Worker] Push Received.');
  console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);

  const title = 'SAILER 알림';
  const options = {
    body: `${event.data.text()}`,
    icon: 'img/icon.png',
    badge: 'img/badge.png'
  };

  event.waitUntil(self.registration.showNotification(title, options));
});
