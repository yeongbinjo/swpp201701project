import time, os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# browser = webdriver.Chrome()
browser = webdriver.Chrome('/usr/local/bin/chromedriver')

url = '{0}:{1}'.format(os.environ.get('HOST', 'localhost'), os.environ.get('PORT', 3000))

# home page
browser.get(url + '/')

time.sleep(1)

userID = 'test_id_0008'
password = 'test0008'
new_password = '1234'

username_mini = browser.find_element_by_id('username_mini')
username_mini.send_keys(userID)
time.sleep(1)

password_mini =browser.find_element_by_id('password_mini')
password_mini.send_keys(password)
time.sleep(1)

loginButton = browser.find_element_by_id('loginButton_mini')
loginButton.click()
time.sleep(1)

InfoTap = browser.find_element_by_id('menu_info')
InfoTap.click()
time.sleep(1)

pwbutton = browser.find_element_by_id('pwchange')
pwbutton.click()
time.sleep(1)

verification = browser.find_element_by_id('verification')
verification.send_keys(password)
time.sleep(1)

confirm = browser.find_element_by_id('confirm')
confirm.click()
time.sleep(1)

new_pw = browser.find_element_by_id('new_pw')
new_pw.send_keys(new_password)
time.sleep(1)

new_pw_confirm = browser.find_element_by_id('new_pw_confirm')
new_pw_confirm.send_keys(new_password)
time.sleep(1)

confirm = browser.find_element_by_id('confirm')
confirm.click()
time.sleep(1)

time.sleep(5)
browser.quit()