from django.conf.urls import url, include
from rest_framework import routers

from quest.views import QuestViewSet

router = routers.DefaultRouter()
router.register(r'quests', QuestViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
]
