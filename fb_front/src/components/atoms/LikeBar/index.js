import React, { PropTypes } from 'react'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'
import FontIcon from 'material-ui/FontIcon';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
import IconLocationOn from 'material-ui/svg-icons/communication/location-on';
import { connect } from 'react-redux'
import { postLike, reviseLike, deleteLike } from '../../../store/like/actions'

const likeIcon = <FontIcon className="material-icons">좋아요</FontIcon>;
const wonderfulIcon = <FontIcon className="material-icons">최고</FontIcon>;
const angryIcon = <FontIcon className="material-icons">화나요</FontIcon>;
const sadIcon = <FontIcon className="material-icons">슬퍼요</FontIcon>;
const funnyIcon = <FontIcon className="material-icons">웃겨요</FontIcon>;
const coolIcon = <FontIcon className="material-icons">멋져요</FontIcon>;

const styles={
  box:{
    display:'flex',
    text:{
      marginLeft:'45px',
      marginRight:'45px'
    }
  }
}

export const LikeBar=({statefunction, likes, postID, loginState, makeLike, changeLike, removeLike, indexfortest}) =>{
  let count=[0,0,0,0,0,0]
  let value
  let id = 0
  if(likes!=undefined){
    for(let like of likes){
      value = 6
      if(like.like=='좋아요')count[0]++
      else if(like.like=='최고에요')count[1]++
      else if(like.like=='화나요')count[2]++
      else if(like.like=='슬퍼요')count[3]++
      else if(like.like=='웃겨요')count[4]++
      else if(like.like=='멋져요')count[5]++

      if(loginState.isLoggedOn){
        if(like.user == loginState.user.id){
          if(like.like =='좋아요')value=0
          else if (like.like=='최고에요')value=1
          else if (like.like=='화나요')value=2
          else if (like.like=='슬퍼요')value=3
          else if (like.like=='웃겨요')value=4
          else if (like.like=='멋져요')value=5
          id = like.id
          break
        }
      }
    }
  }


  const handleLikeClick=()=>{
    if(loginState.isLoggedOn && statefunction.likeState==0){
      let char
      char='좋아요'
      if(value==0){
        removeLike(id,postID)
      }
      else if(id==0){
        makeLike(postID,char)
      }
      else{
        changeLike(id,postID,char)
      }

    }
  }
  const handleWowClick=()=>{
    if(loginState.isLoggedOn && statefunction.likeState==0){
      let char
      char='최고에요'

      if(value==1){
        removeLike(id,postID)
      }
      else if(id==0){
        makeLike(postID,char)
      }
      else{
        changeLike(id,postID,char)
      }
    }
  }

  const handleAngryClick=()=>{
    if(loginState.isLoggedOn && statefunction.likeState==0){
      let char
      char='화나요'
      if(value==2){
        removeLike(id,postID)
      }
      else if(id==0){
        makeLike(postID,char)
      }
      else{
        changeLike(id,postID,char)
      }
    }
  }

  const handleSadClick=()=>{
    if(loginState.isLoggedOn && statefunction.likeState==0){
      let char
      char='슬퍼요'
      if(value==3){
        removeLike(id,postID)
      }
      else if(id==0){
        makeLike(postID,char)
      }
      else{
        changeLike(id,postID,char)
      }

    }
  }
  const handleFunnyClick=()=>{
    if(loginState.isLoggedOn && statefunction.likeState==0){
      let char
      char='웃겨요'
      if(value==4){
        removeLike(id,postID)
      }
      else if(id==0){
        makeLike(postID,char)
      }
      else{
        changeLike(id,postID,char)
      }
    }
  }

  const handleCoolClick=()=>{
    if(loginState.isLoggedOn && statefunction.likeState==0){
      let char
      char='멋져요'
      if(value==5){
        removeLike(id,postID)
      }
      else if(id==0){
        makeLike(postID,char)
      }
      else{
        changeLike(id,postID,char)
      }
    }
  }
  return (
    <Paper zDepth={1}>
      <div style={styles.box}>
        <h3 style={styles.box.text}>{count[0]}</h3>
        <h3 style={styles.box.text}>{count[1]}</h3>
        <h3 style={styles.box.text}>{count[2]}</h3>
        <h3 style={styles.box.text}>{count[3]}</h3>
        <h3 style={styles.box.text}>{count[4]}</h3>
        <h3 style={styles.box.text}>{count[5]}</h3>
      </div>
      <BottomNavigation selectedIndex={value}>
        <BottomNavigationItem
          id={"likeButton_like_"+indexfortest}
          icon={likeIcon}
          onTouchTap={handleLikeClick}
        />
        <BottomNavigationItem
          id={"likeButton_wow_"+indexfortest}
          icon={wonderfulIcon}
          onTouchTap={handleWowClick}
        />
        <BottomNavigationItem
          id={"likeButton_angry_"+indexfortest}
          icon={angryIcon}
          onTouchTap={handleAngryClick}
        />
        <BottomNavigationItem
          id={"likeButton_sad_"+indexfortest}
          icon={sadIcon}
          onTouchTap={handleSadClick}
        />
        <BottomNavigationItem
          id={"likeButton_funny_"+indexfortest}
          icon={funnyIcon}
          onTouchTap={handleFunnyClick}
        />
        <BottomNavigationItem
          id={"likeButton_cool_"+indexfortest}
          icon={coolIcon}
          onTouchTap={handleCoolClick}
        />
      </BottomNavigation>
    </Paper>
  );
}

const mapStateToProps = (state) =>{
  return {
    statefunction : state.like
    }
}
const mapDispatchToProps = (dispatch) => {
  return {
    makeLike: (postID, char)=>{
      dispatch(postLike( postID, char))
    },
    changeLike: (likeID, postID, char)=>{
      dispatch(reviseLike(likeID, postID, char))
    },
    removeLike: (likeID, postId)=>{
      dispatch(deleteLike(likeID,postId))
    }
  }
};

export default connect(mapStateToProps,mapDispatchToProps)(LikeBar)
