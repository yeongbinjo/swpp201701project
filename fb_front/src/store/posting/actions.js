export const textType = (text) => {
  return {
    type: 'TEXT_TPYE',
    text,
  }
}
export const imageChange = (image, imagePreviewURL) => {
  return {
    type: 'IMAGE_CHANGE',
    image,
    imagePreviewURL,
  }
}
export const pushPost = (text, ownerID, tags) => {
  return {
    type: 'PUSH_POST',
    text,
    ownerID,
    tags,
  }
}
export const postSuccess = () => {
  return {
    type: 'POST_SUCCESS',
  }
}
export const afterSuccess = () => {
  return {
    type: 'AFTER_SUCCESS',
  }
}
export const successChecker = () => {
  return {
    type: 'SUCCESS_CHECKER',
  }
}
