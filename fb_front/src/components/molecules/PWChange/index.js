import React, { PropTypes } from 'react';
import { connect } from 'react-redux'
import { pushVerification, success, reset, change } from '../../../store/password/actions'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

 const styles={
  popup:{
    width:"600px",
  },
  textField:{
    marginLeft:"50px",
    width:"350px",
  },
  button:{
    marginTop:"20px",
    // marginLeft:"50px",
  }
 }

class PWChange extends React.Component {
  state = {
    open: false,
  };

  render() {
    let old_pw, new_pw, new_pw2
    const handleOpen = () => {
        this.setState({open: true})
    };
    const verify = () => {
        this.props.onVerify(this.props.user.username, old_pw.input.value)
    };
    const confirm = () => {
        if(new_pw.input.value == new_pw2.input.value){
          this.props.onChange(this.props.user, new_pw.input.value)
          this.props.onReset()
          this.setState({open: false})
        }
        else
          alert('같은 값을 입력해주세요')
    };
    const handleCancel = () => {
        this.props.onReset()
        this.setState({open: false});
    };

    const frame1 = [
      <FlatButton id="confirm" label="확인" primary={true} onClick={verify} />,
      <FlatButton id="cancel" label="취소" primary={true} onTouchTap={handleCancel} />
    ];

    const frame2 = [
      <TextField id="new_pw" style={styles.textField} ref={node=>{new_pw=node;}} type="password" hintText="새로운 비밀번호를 입력해주세요." />,
      <TextField id="new_pw_confirm" style={styles.textField} ref={node=>{new_pw2=node;}} type="password" hintText="비밀번호 확인" />,
      <FlatButton id="confirm" label="확인" primary={true} onClick={confirm} />,
      <FlatButton id="cancel" label="취소" primary={true} onTouchTap={handleCancel} />
    ];

    if(!this.props.verificationState.verified) return (
        <div>
          <RaisedButton style={styles.button} id="pwchange" label="비밀번호 변경" onTouchTap={handleOpen} />
          <Dialog style={styles.popup} title="현재 비밀번호 확인"  actions={frame1} modal={true} open={this.state.open}>
          <TextField id="verification" style={styles.textField} ref={node=>{old_pw=node;}} type="password" hintText="현재 비밀번호를 다시 한 번 입력해주세요." />
          </Dialog>
        </div>
      )
    else return (
      <div>
        <RaisedButton style={styles.button} id="pwchange" label="비밀번호 변경" onTouchTap={handleOpen} />
        <Dialog style={styles.popup} title="새로운 비밀번호 입력"  actions={frame2} modal={true} open={this.state.open}>
        </Dialog>
      </div>
      )
  }
}

const mapStateToProps = (state) =>{
  return {
    verificationState: state.password
  }
}

const mapDispatchToProps= (dispatch)=>{
  return{
    onVerify: (id, pw, num)=>{
      dispatch(pushVerification(id, pw))
    },
    onReset: ()=> {
      dispatch(reset())
    },
    onChange: (user, pw)=>{
      dispatch(change(user, pw))
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(PWChange)