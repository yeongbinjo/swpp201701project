import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import Websocket from 'react-websocket';
import { backendUrl } from 'config';

class ChatAlertSnackbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      from: null,
      message: '',
    };
  }

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  handleActionTouchTap = () => {
    if (this.state.from) {
      this.props.onStartChat(this.state.from);
    }
  };

  handleData(data) {
    let result = JSON.parse(data);
    if (!this.props.chatTarget && result.id) {
      this.setState({
        open: true,
        from: result,
        message: result.last_name + result.first_name + '님이 메시지를 보내셨습니다.',
      })
    }
  }

  render() {
    const url = backendUrl.replace('http://', 'ws://').replace('https://', 'wss://') + '/ws/chat/?token=' + this.props.token;

    return (
      <div>
        <Snackbar
          open={this.state.open}
          message={this.state.message}
          autoHideDuration={4000}
          action="대화하기"
          onActionTouchTap={this.handleActionTouchTap}
          onRequestClose={this.handleRequestClose}
        />
        <Websocket url={url}
                   onMessage={this.handleData.bind(this)}/>
      </div>
    );
  }
}

export default ChatAlertSnackbar
