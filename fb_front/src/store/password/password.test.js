import verificationReducer from './reducer'
import { initialstate } from './selectors'
import totalinitialstate from '../totalinitialstate'

describe('verification Reducer', () => {
  it('has a default state', () => {
    expect(verificationReducer(undefined, { type: 'unexpected' })).toEqual(
      initialstate
    )
  })
  it('start',() =>{
    expect(verificationReducer(undefined,{type:'SUCCESS',token:'abcd', user:'userObject'})).toEqual({
      ...initialstate,
        verified: true,
    })
  })
  it('reset', () => {
    expect(verificationReducer(undefined, { type: 'RESET' })).toEqual(
      initialstate
    )
  })
})
