export const START_CHAT = 'START_CHAT';
export const CLOSE_CHAT = 'CLOSE_CHAT';
export const GET_USER_LIST = 'GET_USER_LIST';
export const SET_USER_LIST = 'SET_USER_LIST';
export const FOLLOW = 'FOLLOW';
export const UNFOLLOW = 'UNFOLLOW';
export const FULLNAME_LIST = 'FULLNAME_LIST';

export const startChat = (user) => {
  return {
    type: START_CHAT,
    user,
  }
}

export const closeChat = () => {
  return {
    type: CLOSE_CHAT,
  }
}

export const getUserList = () => {
  return {
    type: GET_USER_LIST,
  }
}
export const FullnameList = (userList) => {
  return {
    type: FULLNAME_LIST,
    userList,
  }
}
export const setUserList = (userList) => {
  return {
    type: SET_USER_LIST,
    userList,
  }
}

export const follow = (userId, followeeId) => {
  return {
    type: FOLLOW,
    userId,
    followeeId,
  }
}

export const unfollow = (userId, followeeId) => {
  return {
    type: UNFOLLOW,
    userId,
    followeeId,
  }
}
