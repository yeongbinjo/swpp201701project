import json

from channels import Group
from channels.test import ChannelTestCase
from channels.test import HttpClient
from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token


class ChatServerTest(ChannelTestCase):
    def setUp(self):
        self.from_user = get_user_model().objects.create_user(username='from', password='a123456789', first_name='from')
        self.to_user = get_user_model().objects.create_user(username='to', password='a123456789', first_name='to')

    def test_user_can_send_pm_to_himself(self):
        client = HttpClient()
        client.login(username='from', password='a123456789')
        from_token = Token.objects.get(user=self.from_user)
        client.send_and_consume('websocket.connect', path='/ws/chat/?to=1&token=' + from_token.key)
        self.assertIsNone(client.receive())

        Group('message-1-1').send({
            'text': json.dumps({
                'message': 'Hi'
            })
        }, immediately=True)
        res = client.receive()
        self.assertEqual(res['message'], 'Hi')

    def test_user_can_send_pm_to_another_user(self):
        from_client = HttpClient()
        from_client.login(username='from', password='a123456789')
        from_token = Token.objects.get(user=self.from_user)
        from_client.send_and_consume('websocket.connect', path='/ws/chat/?to=2&token=' + from_token.key)
        self.assertIsNone(from_client.receive())

        to_client = HttpClient()
        to_client.login(username='to', password='a123456789')
        to_token = Token.objects.get(user=self.to_user)
        to_client.send_and_consume('websocket.connect', path='/ws/chat/?to=1&token=' + to_token.key)
        self.assertIsNone(to_client.receive())

        Group('message-1-2').send({
            'text': json.dumps({
                'message': 'Hi'
            })
        }, immediately=True)
        res = from_client.receive()
        self.assertEqual(res['message'], 'Hi')
        res = to_client.receive()
        self.assertEqual(res['message'], 'Hi')
