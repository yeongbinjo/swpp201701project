import requests, json
import os
from time import sleep
from random import randint


def post_or_error(link, data):
    sleep(0.05)
    try:
        headers = {'Content-Type': 'application/json;'}
        res = requests.post(link, data=json.dumps(data), headers=headers)
        if res.status_code != 201:
            print("ERROR: Cannot post {0} : {1}".format(link, res.status_code))
            print("maybe that username already exists")
            exit(1)
    except Exception:
        print("ERROR: Cannot post {0}".format(link))
        exit(1)



#post user
link = os.environ.get('BACKEND_URL', 'http://localhost:8000') + "/list/"
userN = 10
print("Checking POST {0} by creating {1} users.".format(link, userN))

for i in range(0, userN):
    user = {}
    user["username"] = "user"+str(i)
    user["password"] = "user"+str(i)*4
    user["last_name"] = str(i)
    user["first_name"] = "test"

    user["birthday"] = "2000-0"+str(randint(1,9))+"-"+str(randint(10,28))
    user["gender"] = randint(1,2)


    print("\tposting new user : {0}".format(user["username"]))
    post_or_error(link, user)




print("TEST SUCCESSFUL")
