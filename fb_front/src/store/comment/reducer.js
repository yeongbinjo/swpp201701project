// may be needed, to implement after post event
//

import { initialstate } from './selectors'

const commentReducer = (state = initialstate, action) => {
  switch (action.type) {
    case 'POST_COMMENT':
      return {
        ...state,
        commentState: 1,
      }
    case 'POST_COMMENT_SUCCESS':
      return {
        ...state,
        commentState: 2,
      }
    case 'AFTER_COMMENT_SUCCESS':
      return {
        ...state,
        commentState: 0,
      }
    default:
      return state
  }
}

export default commentReducer
