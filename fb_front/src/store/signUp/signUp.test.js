import signUpReducer from './reducer'
import * as actions from './actions'
import { initialstate} from './selectors'
import totalinitialstate from '../totalinitialstate'

describe('signUp Reducer',() =>{

  it('has a default state',() =>{
    expect(signUpReducer(undefined,{type:'unexpected'})).toEqual(
      initialstate
    )
  })
  it('get post user request',() =>{
    expect(signUpReducer(undefined,{type:'POST_USER_REQUEST'})).toEqual({
      ...initialstate,
      signUpState:1
    })
  })
  it('user is created',() =>{
    expect(signUpReducer(undefined,{type:'USER_CREATED', username:'hello'})).toEqual({
      ...initialstate,
      username:'hello',
      signUpState:2
    })
  })
  it("create is failed",() =>{
    expect(signUpReducer(undefined,{type:'CREATE_FAIL'})).toEqual({
      ...initialstate,
      signUpState:0
    })
  })
  it('info transfer is done',() =>{
    expect(signUpReducer(undefined,{type:'FINISH_TRANSFER'})).toEqual({
      ...initialstate,
      signUpState:0
    })
  })

})
