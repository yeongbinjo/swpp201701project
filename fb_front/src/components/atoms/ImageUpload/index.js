import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { pushImageChange } from '../../../store/infoChange/actions'
import RaisedButton from 'material-ui/RaisedButton';


const styles = {
  previewComponent:{
    marginLeft: "50px",
  },
  button:{
    marginLeft:"95px",
  },
  fileInput:{
    borderBottom: "4px solid lightgray",
    borderRight: "4px solid lightgray",
    borderTop: "1px solid black",
    borderLeft: "1px solid black",
    padding: "10px",
    margin: "15px",
    cursor: "pointer",
  },
  imgPreview:{
    textAlign: "center",
    margin: "5px 15px",
    width: "200px",
    height: "200px",
    borderLeft: "1px solid gray",
    borderRight: "1px solid gray",
    borderTop: "5px solid gray",
    borderBottom: "5px solid gray",
  },
  img:{
      width:"auto",
      height:"auto",
      maxWidth:"200px",
      maxHeight:"200px",
      // verticalAlign:"middle",
  }
};


export class ImageUpload extends React.Component {
  state = {
    file: '',imagePreviewUrl: '',
  };

  handleSubmit(e) {
    e.preventDefault();
    this.props.onChange(this.props.user, this.state.file, this.state.imagePreviewUrl)
  }

  handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    // console.log("FILE", file)

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }
    reader.readAsDataURL(file)
  }

  render() {
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null
    // console.log("[Image]", this.props.loginState.user.profile_image)


    if (imagePreviewUrl) {
      $imagePreview = (<img style={styles.img} src={imagePreviewUrl} />);
    } else if (this.props.loginState.user.profile_image){
      $imagePreview = <img style={styles.img} src={this.props.loginState.user.profile_image} />
    } else
      $imagePreview = (<div className="previewText">이미지를 등록해주세요</div>);


    return (
      <div style={styles.previewComponent}>
        <form onSubmit={(e)=>this.handleSubmit(e)}>
          <input style={{marginLeft:'20%'}} type="file" onChange={(e)=>this.handleImageChange(e)} />
        </form>
        <div style={styles.imgPreview}>
          {$imagePreview}
        </div>
        <button style={styles.button} className="submitButton" type="submit" onClick={(e)=>this.handleSubmit(e)}>변경</button>
      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    loginState: state.login,
    infoChangeState: state.infoChange
  }
}

const mapDispatchToProps= (dispatch)=>{
  return{
    onChange: (user, image, imagePreviewUrl)=>{
      dispatch(pushImageChange(user, image, imagePreviewUrl))
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ImageUpload)