import React from 'react'
import PropTypes from 'prop-types'
import ShowPost from '../ShowPost'

const style = {
  padding: '16px',
}

const ShowPosts = ({ contents, loginState }) => {
  return (
    <div>
      {contents.map((element, index) =>
        <div key={`key_${element.id}`}style={style}>
          <ShowPost
            id={`postset_${index}`}
            element={element}
            index={index}
            loginState={loginState}
          />
        </div>
      )}
    </div>
  )
}


ShowPosts.propTypes = {
  contents: PropTypes.array,
  loginState: PropTypes.object,
}

export default ShowPosts
