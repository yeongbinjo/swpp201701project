import React from 'react'
import PropTypes from 'prop-types'
import AutoCompleteSearch from '../../molecules/AutoCompleteSearch'
import NewsFeed from '../../organisms/NewsFeed'

class TagPage extends React.Component {
  render() {
    return (
      <div>
        <NewsFeed tag={this.props.params.tag} />
      </div>
    )
  }
}

TagPage.propTypes = {
  params: PropTypes.object,
}

export default TagPage
