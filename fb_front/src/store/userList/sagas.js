import { take, put, call, fork } from 'redux-saga/effects'
import api from 'services/api'
import { backendUrl } from 'config'
import { FULLNAME_LIST, FOLLOW, GET_USER_LIST, SET_USER_LIST, UNFOLLOW } from './actions'
import { GET_NEWSFEEDS } from '../newsFeed/actions'

export function* userList() {
  const response = yield call(api.get, `${backendUrl}/list/`)
  yield put({ type: SET_USER_LIST, userList: response })
}

export function* fullnameList(fullname) {
  const response = yield call(api.get, `${backendUrl}/list/?fullname=${fullname}`)
  console.log(response)
  yield put({ type: FULLNAME_LIST, userList: response })
}

export function* follow(userId, followeeId) {
  const token = JSON.parse(sessionStorage.getItem('loginState')).token
  const response = yield call(api.patch, `${backendUrl}/list/${userId}/`,
    { followees: [followeeId], add_followees: true }, {
      method: 'PATCH',
      mode: 'cors',
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    })
  yield put({ type: 'UPDATE_LOGIN', user: response, token })
  yield put({ type: GET_NEWSFEEDS })
}

export function* unfollow(userId, followeeId) {
  const token = JSON.parse(sessionStorage.getItem('loginState')).token
  const response = yield call(api.patch, `${backendUrl}/list/${userId}/`,
    { followees: [followeeId], add_followees: false }, {
      method: 'PATCH',
      mode: 'cors',
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    })
  yield put({ type: 'UPDATE_LOGIN', user: response, token })
  yield put({ type: GET_NEWSFEEDS })
}

export function* userListFlow() {
  while (true) {
    yield take(GET_USER_LIST)
    yield call(userList)
  }
}

export function* fullnameFlow() {
  while (true) {
    yield take(FULLNAME_LIST)
    yield call(fullnameList)
  }
}

export function* followFlow() {
  while (true) {
    const { userId, followeeId } = yield take(FOLLOW)
    yield call(follow, userId, followeeId)
  }
}

export function* unfollowFlow() {
  while (true) {
    const { userId, followeeId } = yield take(UNFOLLOW)
    yield call(unfollow, userId, followeeId)
  }
}

export default function* () {
  yield fork(userListFlow)
  yield fork(fullnameFlow)
  yield fork(followFlow)
  yield fork(unfollowFlow)
}
