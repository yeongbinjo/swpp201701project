import { initialstate } from './selectors'

const infoReducer = (state = initialstate, action) => {
  switch (action.type) {
    case 'SUCCESS':
      return initialstate;
    case 'TEMP_CHANGE':
        return {
            ...state,
            image : action.image,
            imagePreviewURL : action.imagePreviewURL
        }
    default:
      return state;
  }
}

export default infoReducer
