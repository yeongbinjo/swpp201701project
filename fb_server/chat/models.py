from django.conf import settings
from django.db import models


class Message(models.Model):
    channel = models.TextField(db_index=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    def to_dict(self):
        return {
            'id': self.user.pk,
            'content': self.content,
            'created': str(self.created)
        }
