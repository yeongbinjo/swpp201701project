import { call, put, fork, take, cancel, select } from 'redux-saga/effects'
import api from 'services/api'
import * as actions from './actions'
import { delay } from 'redux-saga'
import { backendUrl } from 'config'
import {browserHistory} from 'react-router';

const url = backendUrl

export function* verificationFlow() {
  while(true){
    const action = yield take('PUSH_VERIFICATION')
    try{
      yield call(api.post, url+'/api-token-auth/',{ username: action.id,password: action.pw})
      yield put({type:'SUCCESS'})
    }
    catch(e){
      console.log(e);
      alert("비밀번호가 일치하지 않습니다.")
    }
  }
}

export function* reset() {
  while(true){
    const action = yield take('RESET')
    yield put({type:'RESET'})
  }
}

export function* change() {
  while(true){
    const action = yield take('CHANGE')
    const response = yield call(fetch, url+'/list/'+action.user.id+'/',{
      method:'PATCH',
      headers:{
        'Accept': 'application/json',
        'Content-Type'  : 'application/json',
      },
      body:JSON.stringify({
        password:action.pw,
      })
    })
    if (response.status==200) alert('비밀번호가 변경되었습니다')
    else alert('에러 발생')
  }
}

export default function* (){
  yield fork(verificationFlow)
  yield fork(reset)
  yield fork(change)
}
