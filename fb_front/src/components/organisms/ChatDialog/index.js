import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Card, CardHeader, CardText } from 'material-ui/Card'
import TextField from 'material-ui/TextField'
import Divider from 'material-ui/Divider'
import Clear from 'material-ui/svg-icons/content/clear'
import { backendUrl } from 'config'
import { SpeechBubble } from '../../atoms/SpeechBubble/index'
import { closeChat } from '../../../store/userList/actions'


const style = {
  wrapper: {
    position: 'relative',
    height: '100%',
  },
  card: {
    height: '100%',
    marginRight: '10px',
    position: 'relative',
  },
  cardText: {
    height: '240px',
    overflowY: 'scroll',
  },
  textField: {
    position: 'absolute',
    bottom: 0,
  },
  closeIcon: {
    position: 'absolute',
    top: '16px',
    right: '16px',
    color: 'rgba(0, 0, 0, 0.6)',
    width: '20px',
    height: '20px',
  },
}

class ChatDialog extends React.Component {
  constructor(props) {
    super(props)

    this.isLeft = this.isLeft.bind(this)
    this.dismiss = this.dismiss.bind(this)
    this.handleData = this.handleData.bind(this)
    this.handleInput = this.handleInput.bind(this)
    this.handleKey = this.handleKey.bind(this)
    this.ws = null

    this.state = {
      messages: [],
      input: '',
      ws: null,
    }
  }

  componentDidMount() {
    const url = `${backendUrl.replace('http://', 'ws://').replace('https://', 'wss://')}/ws/chat/?to=${this.props.to.id}&token=${this.props.token}`
    this.ws = new WebSocket(url)
    this.ws.onmessage = this.handleData
  }

  componentWillUnmount() {
    if (this.ws) {
      this.ws.close()
    }
  }

  handleData(data) {
    const result = JSON.parse(data.data)
    const copy = this.state.messages.slice()
    const body = document.querySelector('#chat-body')
    copy.push(result)
    if (copy.length > 20) {
      copy.shift()
    }
    this.setState({
      messages: copy,
    })
    body.scrollTop = body.scrollHeight
  }

  handleInput(event, value) {
    this.setState({
      input: value,
    })
  }

  handleKey(event, value) {
    if (event.charCode === 13 && this.state.input !== '') {
      this.ws.send(JSON.stringify({
        message: this.state.input,
      }))
      event.target.value = ''
      this.setState({
        input: '',
      })
    }
  }

  render() {
    const name = this.props.to.last_name + this.props.to.first_name
    const bubbles = []
    for (let i = 0; i < this.state.messages.length; i++) {
      const message = this.state.messages[i]
      bubbles.push(
        <SpeechBubble
          key={i}
          message={message.content}
          left={this.isLeft(message.id)}
        />
      )
    }

    let avatarOrDefault = this.props.to.profile_image ? this.props.to.profile_image : '/img/avatar.png'
    if (avatarOrDefault.substring(0, 2) === '/m') avatarOrDefault = backendUrl + avatarOrDefault

    return (<div style={style.wrapper}>
      <Card style={style.card}>
        <CardHeader
          title={name}
          avatar={avatarOrDefault}
        />
        <Clear style={style.closeIcon} onTouchTap={this.dismiss} />
        <Divider />
        <CardText id="chat-body" style={style.cardText}>
          {bubbles}
        </CardText>
        <TextField
          style={style.textField} hintText="친구에게 당신의 말을 전하세요."
          onChange={this.handleInput}
          onKeyPress={this.handleKey}
        />
      </Card>
    </div>)
  }

  isLeft(id) {
    return id !== this.props.me.id
  }

  dismiss() {
    this.props.closeChat()
  }
}

ChatDialog.propTypes = {
  me: PropTypes.object,           /* 로그인 된 유저 자기 자신의 정보를 담고 있는 객체 */
  to: PropTypes.object,           /* 채팅 메세지를 전달하는 대상 유저 객체 */
  token: PropTypes.string,
  closeChat: PropTypes.func,
}

const mapStateToProps = () => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    closeChat: () => {
      dispatch(closeChat())
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatDialog)
