import requests, json
import os
from time import sleep
from random import randint


def get_login_token(username, passwd):
    url = 'http://localhost:8000/api-token-auth/'
    res = requests.post(url, data={'username': username, 'password': passwd})
    if res.status_code != 200:
        raise ValueError
    return json.loads(res.text)['token']


def get_or_error(link):
    sleep(0.05)
    try:
        res = requests.get(link)
        if res.status_code != 200:
            print("ERROR: Cannot get {0} : {1}".format(lin, res.status_code))
            exit(1)
    except Exception:
        print("ERROR: Cannot get {0}".format(link))
        exit(1)
    return res.json()


def post_or_error(link, data):
    sleep(0.05)
    try:
        res = requests.post(link, data=json.dumps(data), headers={
            'Content-Type': 'application/json'
        })
        if res.status_code != 201:
            print("ERROR: Cannot post {0} : {1}".format(link, res.status_code))
            exit(1)
    except Exception:
        print("ERROR: Cannot post {0}".format(link))
        exit(1)
    return res.json()


def post_or_error_with_auth(link, data, name, pswd):
    sleep(0.05)
    print(data)
    try:
        res = requests.post(link, data=json.dumps(data), headers={
            'Authorization': 'Token %s' % get_login_token(name, pswd),
            'Content-Type': 'application/json'
        })
        print(res.json())
        if res.status_code != 201:
            print("ERROR: Cannot post {0} : {1}".format(link, res.status_code))
            exit(1)
    except Exception as e:
        print(e)
        print("ERROR: Cannot post {0}".format(link))
        print(e)
        exit(1)
    return res.json()


#post user
# link = os.environ.get('BACKEND_URL', 'http://localhost:8000') + "/list/"
# userN = 2
# print("Checking POST {0} by creating {1} users.".format(link, userN))
# user_list = []
# post_list = []
# for i in range(1, userN+1):
#     link = os.environ.get('BACKEND_URL', 'http://localhost:8000') + "/list/"
#     user = {}
#     user["username"] = "TESTUSER"+str(i)
#     user["password"] = "user"+str(i)*4
#     user["last_name"] = str(i)
#     user["first_name"] = "test"

#     user["birthday"] = "1993-0"+str(randint(1,9))+"-"+str(randint(10,28))
#     user["gender"] = randint(1,2)


#     print("\tposting new user : {0}".format(user["username"]))
#     user_data = post_or_error(link, user)
#     user_list.append(user_data)
user={}
user['username']='USER1'
user['password'] = 'user1111'
    # link = os.environ.get('BACKEND_URL', 'http://localhost:8000') + "/posts/"
    # post = 3
    # print("Checking POST {0} by creating {1} posts.".format(link, post))
    # print("Posting user's ID is {0}.".format(user['username']))
    # print("Creating 3 posts....")

    # for i in range(post):
        # link = os.environ.get('BACKEND_URL', 'http://localhost:8000') + "/posts/"
        # data = {}
        # data['content'] = str(i)*15
        # data['owner'] = user_data['id']
        # post_data = post_or_error_with_auth(link, data, 'TEST1', 'user1111')
        # print("Created post :", data['content'])
        # post_list.append(post_data)
link = 'http://localhost:8000/likes/'
data = {}
data['like'] = '좋아요'
# data['content'] = 'hi'
# data['owner'] = 20
data['post'] = 16
data['user'] = 20
post_or_error_with_auth(link,data,'USER1','user1111')
#         link = os.environ.get('BACKEND_URL', 'http://localhost:8000') + "/likes/"

#         like_list = ["좋아요","웃겨요","화나요","슬퍼요","최고에요","멋져요"]
#         data = {}
#         data['user'] = user_data['id']
#         data['like'] = like_list[randint(0,5)]
#         data['post'] = post_data['id']
#         like_data = post_or_error_with_auth(link, data, user['username'], user['password'])
#         print("{0} posted {1} for post:{2}".format(user_data['username'],like_data['like'],post_data['content']))
# sleep(1)
# print("Now we are gonna test GET likes")

# print("Matching the ID....")
# for i in range(2):
#     link = os.environ.get('BACKEND_URL', 'http://localhost:8000') + "/likes/?user="+str(user_list[i]['id'])
#     data = get_or_error(link)

#     sleep(1)
#     for j in range(len(data)):

#         if data[j]['user'] != user_list[i]['id']:
#             print("Does not match id")
#             exit(1)

#     link = os.environ.get('BACKEND_URL', 'http://localhost:8000') + "/likes/?post="+str(post_list[i]['id'])
#     data = get_or_error(link)

#     sleep(1)
#     for j in range(len(data)):
#         if data[j]['post'] != post_list[i]['id']:
#             print("Does not match id")
#             exit(1)

print("TEST SUCCESSFUL")
