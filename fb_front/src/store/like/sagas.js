import { call, put, fork, take, cancel, select } from 'redux-saga/effects'
import api from 'services/api'
import * as actions from './actions'
import { delay } from 'redux-saga'
import { getLoginState, getToken } from '../login/selectors'
import { getLikeState } from './selectors'
import { backendUrl } from 'config'

const url = backendUrl

export function* reviseLike(likeID,postID,char){
  const loginState = JSON.parse(sessionStorage.getItem('loginState'))
  const token = loginState.token
  const response = yield call(fetch, url+'/likes/'+likeID+'/',{
    method:'DELETE',
    headers:{
      'Accept': 'application/json',
      'Content-Type'  : 'application/json',
      'Authorization': 'Token '+token
    },
  })
  yield call(postLike,postID,char)
}

export function* deleteLike(likeID, postID){
  const loginState = JSON.parse(sessionStorage.getItem('loginState'))
  const token = loginState.token
  const response = yield call(fetch, url+'/likes/'+likeID+'/',{
    method:'DELETE',
    headers:{
      'Accept': 'application/json',
      'Content-Type'  : 'application/json',
      'Authorization': 'Token '+token
    },
  })
  if(response.status==204){
    yield put({type:'LIKE_SUCCESS'})
    yield put({type:'LIKE_SUCCESS_CHECKER'})
    yield put({type:'WALL_PARTIAL_UPDATE',updateType:2,id:postID})
  }
}


export function* postLike(postID,char){
  const loginState = JSON.parse(sessionStorage.getItem('loginState'))
  const userID = loginState.user.id
  const token = loginState.token
  const response = yield call(fetch, url+'/likes/',{
    method:'POST',
    headers:{
      'Accept': 'application/json',
      'Content-Type'  : 'application/json',
      'Authorization': 'Token '+token
    },
    body:JSON.stringify({
      user: userID,
      post:postID,
      like:char,
    })
  })
  if(response.status == 201){
    yield put({type:'LIKE_SUCCESS'})
    yield put({type:'LIKE_SUCCESS_CHECKER'})
    yield put({type:'WALL_PARTIAL_UPDATE',updateType:2,id:postID})
  }
}

export function* likeUpdateFlow(){
  while(true){
    const action =yield take(['POST_LIKE','REVISE_LIKE','DELETE_LIKE'])
    if(action.type=='POST_LIKE'){
      yield call(postLike, action.postID, action.char)
    }
    else if(action.type=='REVISE_LIKE'){
      yield call(reviseLike, action.likeID, action.postID, action.char)
    }
    else{
      yield call(deleteLike, action.likeID, action.postID)
    }
  }
}

export function* likeSuccessCheck(){
  const likeState= yield select(getLikeState)
  if(likeState==2) yield put({type:'AFTER_LIKE_SUCCESS'})
  else yield put({type:'LIKE_SUCCESS_CHECKER'})
}


export function *likeSuccesCheckFlow(){
  while(true){
    yield take('LIKE_SUCCESS_CHECKER')
    yield call(likeSuccessCheck)
  }
}

export default function* (){
  yield fork(likeUpdateFlow)
  yield fork(likeSuccesCheckFlow)
}
