import React from 'react'
import { storiesOf } from '@kadira/storybook'
import { Wall } from 'components'

storiesOf('Wall', module)
  .add('default', () => (
    <Wall />
  ))
  .add('reverse', () => (
    <Wall reverse />
  ))
