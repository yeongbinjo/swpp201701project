export const GET_NEWSFEEDS = 'GET_NEWSFEEDS'
export const SET_NEWSFEEDS = 'SET_NEWSFEEDS'
export const UPDATE_TAGS = 'UPDATE_TAGS'

export const getNewsFeeds = (tag) => {
  return {
    type: GET_NEWSFEEDS,
    tag,
  }
}

export const setNewsFeeds = (newsFeeds, tag) => {
  return {
    type: SET_NEWSFEEDS,
    newsFeeds,
    tag,
  }
}

export const updateTags = (postId, tags) => {
  return {
    type: UPDATE_TAGS,
    postId,
    tags,
  }
}
export const newsfeedReset = () =>{
  return{
    type:'NEWS_FEED_RESET'
  }
}
