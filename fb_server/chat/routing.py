from channels.routing import route_class

from chat.consumers import ChatServer

chat_routing = [
    route_class(ChatServer),
]