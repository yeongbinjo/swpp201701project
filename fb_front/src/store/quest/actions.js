export const questListRequest = () => {
    return {
        type: 'QUEST_LIST_REQUEST',
    }
}

export const questListGet = (questList) =>{
  return{
    type:'QUEST_LIST_GET',
    questList
  }
}
export const userQuestUpdate = (questID ) =>{
  return{
    type:'USER_QUEST_UPDATE',
    questID
  }
}
export const questDoneRequest = (questID) =>{
  return{
    type:'QUEST_DONE_REQUEST',
    questID
  }
}

export const questReload = () =>{
  return{
    type:'QUEST_RELOAD'
  }
}
