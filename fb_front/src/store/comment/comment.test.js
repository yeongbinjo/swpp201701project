import commentReducer from './reducer'
import * as actions from './actions'
import { initialstate, getCommentState } from './selectors'
import totalinitialstate from '../totalinitialstate'

describe('Comment Reducer',() =>{

  it('has a default state',() =>{
    expect(commentReducer(undefined,{type:'unexpected'})).toEqual(
      initialstate
    )
  })
  it('after POST trial',() =>{
    expect(commentReducer(undefined,{type:'POST_COMMENT', index:1, text:'hi'})).toEqual({
      ...initialstate,
      commentState:1
    })
  })
  it('after POST success',() =>{
    expect(commentReducer(undefined,{type:'POST_COMMENT_SUCCESS'})).toEqual({
      ...initialstate,
      commentState:2
    })
  })
  it('renew state',() =>{
    expect(commentReducer(undefined,{type:'AFTER_COMMENT_SUCCESS'})).toEqual({
      ...initialstate,
      commentState:0
    })
  })
  it('getCommentState',()=>{
    expect(getCommentState(totalinitialstate)).toEqual(0)
  })
})
