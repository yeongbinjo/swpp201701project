import { initialstate } from './selectors'
import { subscribeUser } from './webpush'

const loginReducer = (state = initialstate, action) => {
  switch (action.type) {
    case 'UPDATE_LOGIN':
      sessionStorage.setItem('loginState', JSON.stringify({ isLoggedOn: true, token: action.token, user: action.user }))
      subscribeUser()
      return {
        ...state,
        isLoggedOn: true,
        token: action.token,
        user: {
          ...action.user,
          visited: JSON.parse(action.user.visited),
        },
      }
    case 'PUSH_LOGOUT':
      return initialstate

    case 'UPDATE_USERINFO':
      return {
          ...state,
          user: action.user,
      }

    default:
      return state
  }
}

export default loginReducer
