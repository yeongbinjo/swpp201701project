import React from 'react'
import { storiesOf } from '@kadira/storybook'
import Profile from '.'

storiesOf('Profile', module)
  .add('default', () => (
    <Profile>Hello</Profile>
  ))
  .add('reverse', () => (
    <Profile reverse>Hello</Profile>
  ))
