import React from 'react'
import PropTypes from 'prop-types'
import WorldMap from '../../atoms/WorldMap'
import { connect } from 'react-redux'


const styles={
  text:{
    paddingLeft:'100px',
    fontSize:'40px',
    fontFamily: 'Arial',
    fontWeight: '700',
  }
}


const Chronicle=({wallState, loginState})=> {
  return (
    <div>
      <p style={styles.text}>{wallState.wallOwner.first_name+"님! 어디로 떠나볼까요?"} </p>
      <WorldMap visited={wallState.wallOwner.visited}/>
    </div>
  )
}

Chronicle.propTypes = {
  params: PropTypes.object,
}

const mapStateToProps = (state) => {
  return {
    wallState: state.wall,
    loginState: state.login,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Chronicle)
