import { take, put, call, fork } from 'redux-saga/effects'
import api from 'services/api'
import { backendUrl } from 'config'
import { GET_NEWSFEEDS, SET_NEWSFEEDS, UPDATE_TAGS } from './actions'

export function* newsFeed(tag) {
  const loginState = sessionStorage.getItem('loginState')
  const tagName = !tag ? '' : tag
  let response
  if (!loginState || loginState === 'null') {
    response = yield call(api.get, `${backendUrl}/newsfeeds/?tag=${tagName}`)
  } else {
    const token = JSON.parse(loginState).token
    response = yield call(api.get, `${backendUrl}/newsfeeds/?tag=${tagName}`, {
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        Authorization: `Token ${token}`,
      },
    })
  }
  yield put({ type: SET_NEWSFEEDS, newsFeeds: response, tag:tag })
}

export function* newsFeedFlow() {
  while (true) {
    const action = yield take(GET_NEWSFEEDS)
    yield call(newsFeed, action.tag)
  }
}

export function* updateTags(postId, tags) {
  const token = JSON.parse(sessionStorage.getItem('loginState')).token
  yield call(api.patch, `${backendUrl}/posts/${postId}/`,
    { tags }, {
      method: 'PATCH',
      mode: 'cors',
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    })
  yield put({ type: GET_NEWSFEEDS })
}

export function* updateTagsFlow() {
  while (true) {
    const action = yield take(UPDATE_TAGS)
    yield call(updateTags, action.postId, action.tags)
  }
}

export default function* () {
  yield fork(newsFeedFlow)
  yield fork(updateTagsFlow)
}
