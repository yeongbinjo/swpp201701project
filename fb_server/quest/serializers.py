from django.contrib.auth import get_user_model
from rest_framework import serializers

from quest.models import Quest



class QuestSerializer(serializers.ModelSerializer):

    class Meta:
        model = Quest
        fields = ('id', 'name', 'regions', 'explanation', 'created', 'modified', 'image', 'reward')
