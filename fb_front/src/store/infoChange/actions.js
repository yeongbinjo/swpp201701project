export const pushImageChange = (user, image) =>{
  return{
    type:'PROFILE_IMAGE_CHANGE',
    user,
    image
  }
}

export const tempChange = (image, imagePreviewURL) =>{
  return{
    type:'TEMP_CHANGE',
    image,
    imagePreviewURL
  }
}

export const themeChangeRequest = (theme)=>{
  return{
    type:'THEME_CHANGE_REQUEST',
    theme
  }
}
