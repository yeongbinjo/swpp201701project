import React from 'react'
import { storiesOf } from '@kadira/storybook'
import { ShowPosts } from 'components'

storiesOf('ShowPosts', module)
  .add('default', () => (
    <ShowPosts>Hello</ShowPosts>
  ))
  .add('reverse', () => (
    <ShowPosts reverse>Hello</ShowPosts>
  ))
