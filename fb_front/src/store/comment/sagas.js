import { call, put, fork, take, select } from 'redux-saga/effects'
import api from 'services/api'
import { backendUrl } from 'config'
import { getCommentState } from './selectors'
import { GET_NEWSFEEDS } from '../newsFeed/actions'

export function* commentPosting(index, text) {
  const token = JSON.parse(sessionStorage.getItem('loginState')).token
  const response = yield call(api.post, `${backendUrl}/comments/`,
    { post: index, content: text }, {
      method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    })
  if (response.id) {
    yield put({ type: 'POST_COMMENT_SUCCESS' })
    yield put({ type: 'COMMENT_SUCCESS_CHECKER' })
    yield put({ type: 'WALL_PARTIAL_UPDATE', updateType: 0, id: response.post.owner })
    yield put({ type: GET_NEWSFEEDS })
  }
}

export function* commentFlow() {
  while (true) {
    const { index, text } = yield take('POST_COMMENT')
    yield call(commentPosting, index, text)
  }
}

export function* commentSuccessCheck() {
  const commentState = yield select(getCommentState)
  if (commentState === 2) yield put({ type: 'AFTER_COMMENT_SUCCESS' })
  else yield put({ type: 'COMMENT_SUCCESS_CHECKER' })
}


export function* commentSuccesCheckFlow() {
  while (true) {
    yield take('COMMENT_SUCCESS_CHECKER')
    yield call(commentSuccessCheck)
  }
}

export default function* () {
  yield fork(commentFlow)
  yield fork(commentSuccesCheckFlow)
}
