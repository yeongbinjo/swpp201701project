import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Posting from '../../molecules/Posting'
import Profile from '../../atoms/Profile'
import ShowPosts from '../../molecules/ShowPosts'
import { updateLogin } from '../../../store/login/actions'
import { ownerSet } from '../../../store/wall/actions'

const styles = {
  wall: {
    overflow: 'hidden',
    height: '150%',
  },
  elements: {
    width: '100%',
  },
  profile: {
    padding: '16px',
  },
  posting: {
    padding: '16px',
  },
}

export class Wall extends React.Component {
  render() {
    if (this.props.wallState.getState === 2) {
      return (
        <div>
          <div style={styles.profile}>
            <Profile user={this.props.wallState.wallOwner} />
          </div>
          <div style={styles.posting}>
            <Posting
              loginState={this.props.loginState}
              ownerID={this.props.wallState.wallOwner.id}
              ownerFirstName={this.props.wallState.wallOwner.first_name}
            />
          </div>
          <div style={styles.wall}>
            <div style={styles.elements}>
              <ShowPosts
                contents={this.props.wallState.wallPosts}
                loginState={this.props.loginState}
              />
            </div>
          </div>
        </div>
      )
    }
    return (<div />)
  }
}

Wall.propTypes = {
  reverse: PropTypes.bool,
  wallState: PropTypes.object,
  loginState: PropTypes.object,
  params: PropTypes.object,
  ownerSet: PropTypes.func,
}

const mapStateToProps = (state) => {
  return {
    wallState: state.wall,
    loginState: state.login,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginSet: (token, account) => {
      dispatch(updateLogin(token, account))
    },
    ownerSet: (wallOwnerName) => {
      dispatch(ownerSet(wallOwnerName))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Wall)
