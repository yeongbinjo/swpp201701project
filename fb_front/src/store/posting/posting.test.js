import postReducer from './reducer'
import * as actions from './actions'
import { initialstate, getPostState} from './selectors'
import totalinitialstate from '../totalinitialstate'

describe('posting Reducer',() =>{

  it('has a default state',() =>{
    expect(postReducer(undefined,{type:'unexpected'})).toEqual(
      initialstate
    )
  })
  it('push post',() =>{
    expect(postReducer(undefined,{type:'PUSH_POST',text:'hello', ownerID:'1'})).toEqual({
      ...initialstate,
      postState:1
    })
  })
  it('post success when we request post',() =>{
    expect(postReducer({...initialstate,postState:1},{type:'POST_SUCCESS'})).toEqual({
      ...initialstate,
      postState:2
    })
  })
  it("post success when we don't request post",() =>{
    expect(postReducer({...initialstate},{type:'POST_SUCCESS'})).toEqual({
      ...initialstate,
      postState:0
    })
  })
  it('renew',() =>{
    expect(postReducer(undefined,{type:'AFTER_SUCCESS'})).toEqual({
      ...initialstate,
      postState:0
    })
  })
  it('text typing',() =>{
    expect(postReducer(undefined,{type:'TEXT_TPYE',text:'blahblah'})).toEqual({
      ...initialstate,
      text:'blahblah'
    })
  })
  it('image changing',() =>{
    expect(postReducer(undefined,{type:'IMAGE_CHANGE',image:'newImage.jpeg',imagePreviewURL:'preview.jpeg'})).toEqual({
      ...initialstate,
      image:'newImage.jpeg',
      imagePreviewURL:'preview.jpeg'
    })
  })

  it('getPostState',()=>{
    expect(getPostState(totalinitialstate)).toEqual(0)
  })
})
