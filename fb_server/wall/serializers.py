from django.contrib.auth import get_user_model
from rest_framework import serializers

from accounts.serializers import UserSerializer
from wall.models import Post, Comment, Like, Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('name',)


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'post', 'content', 'author', 'created', 'modified')
        depth = 1


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('id', 'user', 'like', 'post')


# http://stackoverflow.com/questions/13921654/django-rest-framework-create-nested-objects-models-by-post
# http://stackoverflow.com/questions/15883678/django-rest-framework-different-depth-for-post-put
class PostWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        exclude = ()


class PostSerializer(serializers.ModelSerializer):
    owner = UserSerializer()
    author = UserSerializer()
    comments = CommentSerializer(many=True)
    likes = LikeSerializer(many=True)

    class Meta:
        model = Post
        fields = ('id', 'content', 'owner', 'author', 'created', 'modified', 'image', 'likes', 'tags', 'comments',
                  'location')
        depth = 1
