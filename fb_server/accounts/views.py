from accounts.serializers import UserSerializer
from rest_framework import status, viewsets, filters
from rest_framework.decorators import list_route
from rest_framework.response import Response
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
User = get_user_model()


class AccountList(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filter_fields = ['username', 'first_name']

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save()

    @list_route(methods=['get'], url_path='username/(?P<username>\w+)')
    def get_by_username(self, request, username):
        user = get_object_or_404(User, username=username)
        return Response(UserSerializer(user).data, status=status.HTTP_200_OK)

    def get_queryset(self):
        fullname = self.request.query_params.get('fullname','')
        if not fullname:
            return User.objects.filter()
        else:
            users = User.objects.all()
            qs = User.objects.all()
            fullname = fullname.replace(" ", "")
            for user in users:
                name = user.last_name + user.first_name
                if name == fullname:
                    qs = qs.filter(Q(last_name=user.last_name, first_name=user.first_name))
            return qs

