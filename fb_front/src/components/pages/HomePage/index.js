import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import NewsFeed from '../../organisms/NewsFeed'
import SignUpPage from '../SignUpPage'

const style = {
  height: '100%',
  minHeight: '100% !important',
}

const HomePage = ({ loginState }) => {
  if (loginState.isLoggedOn) {
    return (
      <div style={style}>
        <NewsFeed />
      </div>
    )
  } return (
    <div style={style}>
      <SignUpPage />
    </div>
  )
}

HomePage.propTypes = {
  loginState: PropTypes.object,
}

const mapStateToProps = (state) => {
  return {
    loginState: state.login,
  }
}

export default connect(mapStateToProps)(HomePage)
