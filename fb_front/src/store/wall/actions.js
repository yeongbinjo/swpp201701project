export const ownerSet = (wallOwnerName) => {
  return {
    type: 'OWNER_SET',
    wallOwnerName,
  }
}

export const setSuccess = (wallOwner, wallPosts) => {
  return {
    type: 'SET_SUCCESS',
    wallOwner,
    wallPosts,
  }
}

export const setFailed = () => {
  return {
    type: 'SET_FAILED',
  }
}

export const wallPartialUpdate = (updateType, id) => {
  return {
    type: 'WALL_PARTIAL_UPDATE',
    updateType,
    id,
  }
}

export const partialPostUpdate = (wallPosts) => {
  return {
    type: 'PARTIAL_POST_UPDATE',
    wallPosts,
  }
}

export const partialCommentUpdate = (comments, postID) => {
  return {
    type: 'PARTIAL_COMMENT_UPDATE',
    comments,
    postID,
  }
}
