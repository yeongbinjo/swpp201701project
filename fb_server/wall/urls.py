from django.conf.urls import url, include
from rest_framework import routers

from wall.views import PostViewSet, CommentViewSet, LikeViewSet, NewsFeedViewSet

router = routers.DefaultRouter()
router.register(r'posts', PostViewSet)
router.register(r'comments', CommentViewSet)
router.register(r'likes', LikeViewSet)
router.register(r'newsfeeds', NewsFeedViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
