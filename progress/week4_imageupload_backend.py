import requests, json
import os
from time import sleep
from random import randint


def get_login_token(username, passwd):
    url = os.environ.get('BACKEND_URL', 'http://localhost:8000') + '/api-token-auth/'
    res = requests.post(url, data={'username': username, 'password': passwd})
    if res.status_code != 200:
        raise ValueError
    return json.loads(res.text)['token']


def get_or_error_with_auth(link, name, pswd):
    sleep(0.05)
    try:
        res = requests.get(link, headers={'Authorization': 'Token %s' % get_login_token(name, pswd)})
        if res.status_code != 200:
            print("ERROR: Cannot post {0} : {1}".format(lin, res.status_code))
            exit(1)
    except Exception:
        print("ERROR: Cannot post {0}".format(link))
        exit(1)
    return res.json()


def post_or_error(link, data):
    sleep(0.05)
    try:
        res = requests.post(link, data=json.dumps(data), headers={
            'Authorization': 'Token %s' % get_login_token(name, pswd),
            'Content-Type': 'application/json'
        })
        if res.status_code != 201:
            print("ERROR: Cannot post {0} : {1}".format(link, res.status_code))
            exit(1)
    except Exception:
        print("ERROR: Cannot post {0}".format(link))
        exit(1)


def post_or_error_with_auth(link, data, name, pswd, image):
    sleep(0.05)

    response = requests.get(image,stream=True)

    file = (response.raw.read())
    lenlen = len(file)
    files = {
        'image': ('trtr.jpg',file),
        'Content-Type': 'image/jpg',
    }
    res = requests.post(link, data={'owner':'1','content':data}, files=files, headers={
        'Authorization': 'Token %s' % get_login_token(name, pswd),
        # 'Content-Type': 'application/json'
    })
    if res.status_code != 201:
        print("ERROR: Cannot post {0} : {1}".format(link, res.status_code))
        exit(1)
    '''
    except Exception as e:
        print(e)
        print("ERROR: Cannot post {0}".format(link))
        exit(1)
    '''

'''---------test code---------'''

'''------ 글쓰기 -------'''
user={}
user['username'] = 'aksdmj'
user['password'] = 'qwer1234'

backend_url = os.environ.get('BACKEND_URL', 'http://localhost:8000')
link = backend_url + '/posts/'
post = 3
imageList =["http://dl.dongascience.com/uploads/article/Contents/201609/S201610N060_1.jpg", "http://sf.snu.ac.kr/gil.hur/images/chungkil-hur.jpg","http://cfs3.tistory.com/upload_control/download.blog?fhandle=YmxvZzY0MTk5QGZzMy50aXN0b3J5LmNvbTovYXR0YWNoLzAvMjYuanBn"]
print("Checking POST {0} by creating {1} posts with images.".format(link, post))
print("Posting user's ID is {0}.".format(user['username']))


for i in range(post):
    data = {}
    data['content'] = str(i)*15
    post_or_error_with_auth(link, data, user['username'], user['password'], imageList[i])
    print("Created post :", data['content'])

print("Created 10 posts successfully!")


'''--------- 담벼락 글 가져오기 ----------'''
link = backend_url + '/posts/?user=28'

print("Now let's check GET {0} by matching user's id with post owner's id")
print("Getting all posts of user_id : user0....")
sleep(1)

data = get_or_error_with_auth(link, user['username'], user['password'])

print("Matching the ID....")
sleep(1)
for i in range(len(data)):
    if data[i]['author']['id'] != 'aksdmj':
        print("Does not match id")
        exit(1)

print("The owner's ID of every post that we've gotten is matched with user's ID")

print("TEST SUCCESSFUL")
