export const postUserRequest = (username, lastname, firstname, password, gender, birthday) => {
    return {
        type: 'POST_USER_REQUEST',
        username,
        lastname,
        firstname,
        password,
        gender,
        birthday
    }
}

export const userCreated = (username) =>{
  return{
    type:'USER_CREATED',
    username
  }
}

export const createFail = () =>{
  return{
    type: 'CREATE_FAIL',
  }
}

export const finishTransfer=()=>{
  return{
    type: 'FINISH_TRANSFER'
  }
}
