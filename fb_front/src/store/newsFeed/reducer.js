import { initialstate } from './selectors'
import { SET_NEWSFEEDS } from './actions'

const newsFeedReducer = (state = initialstate, action) => {
  switch (action.type) {
    case SET_NEWSFEEDS:
      return {
        ...state,
        newsFeeds: action.newsFeeds,
        tag:action.tag,
        isDefault:0,
      }
    case "NEWSFEED_RESET":
      return initialstate
    default:
      return state
  }
}

export default newsFeedReducer
