import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { backendUrl } from 'config'
import { Card, CardActions, CardHeader, CardMedia, CardText } from 'material-ui/Card'
import FlatButton from 'material-ui/FlatButton'
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import moment from 'moment'
import { getNewsFeeds, updateTags } from '../../../store/newsFeed/actions'

class NewsFeedPost extends React.Component {
  static contextTypes = {
    router: PropTypes.object,
  }

  constructor() {
    super()

    this.handleChange = this.handleChange.bind(this)
    this.onTagClick = this.onTagClick.bind(this)
  }

  onTagClick(tag) {
    this.context.router.push(`/tag/${tag}`)
    this.props.getNewsFeeds(tag)
  }

  handleChange(tags) {
    this.props.updateTags(this.props.post.id, tags)
  }

  render() {
    const imageUrl = `${backendUrl}${this.props.post.image}`
    const media = this.props.post.image ? (
      <CardMedia>
        <img alt="이미지" src={imageUrl} />
      </CardMedia>
    ) : (<div />)
    const tags = this.props.post.tags.map((tagObj) => { return tagObj.name })

    const authorFullname = `${this.props.post.author.last_name}${this.props.post.author.first_name}`
    const ownerFullname = `${this.props.post.owner.last_name}${this.props.post.owner.first_name}`
    const title = this.props.post.author.id === this.props.post.owner.id ? authorFullname : `${authorFullname} → ${ownerFullname}`

    const customRenderTag = (props) => {
      const { tag, key, disabled, onRemove, classNameRemove, getTagDisplayValue, ...other } = props
      return (
        <span key={key} {...other}>
          <span onClick={(e) => this.onTagClick(tag)}>
            {getTagDisplayValue(tag)}
          </span>
          {!disabled && <a className={classNameRemove} onClick={(e) => onRemove(key)} />}
        </span>
      )
    }

    return (
      <Card>
        <CardHeader
          title={title}
          subtitle={moment(this.props.post.modified).locale('ko').fromNow()}
          avatar="/img/avatar.png"
        />
        {media}
        <CardText>
          {this.props.post.content}
        </CardText>

        <CardText>
          <TagsInput value={tags} onChange={this.handleChange} renderTag={customRenderTag} />
        </CardText>

        <CardActions>
          <FlatButton label="댓글달기" />
          <FlatButton label="좋아요" />
        </CardActions>
      </Card>
    )
  }
}

NewsFeedPost.propTypes = {
  post: PropTypes.object,
  updateTags: PropTypes.func,
  getNewsFeeds: PropTypes.func,
}

const mapStateToProps = () => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateTags: (postId, tags) => {
      dispatch(updateTags(postId, tags))
    },
    getNewsFeeds: (tag) => {
      dispatch(getNewsFeeds(tag))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsFeedPost)
