const merge = require('lodash/merge')

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    apiUrl: 'https://jsonplaceholder.typicode.com',
    backendUrl: process.env.BACKEND_URL || 'http://54.187.174.208:8000'
  },
  test: {},
  development: {},
  production: {
    apiUrl: 'https://jsonplaceholder.typicode.com',
  },
}

process.title = 'swpp2017project'
module.exports = merge(config.all, config[config.all.env])
