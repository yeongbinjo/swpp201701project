import React from 'react'
import PropTypes from 'prop-types'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import RemoveRedEye from 'material-ui/svg-icons/image/remove-red-eye'
import PersonAdd from 'material-ui/svg-icons/social/person-add'
import ContentLink from 'material-ui/svg-icons/content/link'
import ContentCopy from 'material-ui/svg-icons/content/content-copy'
import Divider from 'material-ui/Divider'
import Subheader from 'material-ui/Subheader'


class MenuBar extends React.Component {
  static contextTypes = {
    router: PropTypes.object,
  }

  render() {
    const handleWallGo = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push(`/wall/${this.props.loginState.user.username}`)
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }
    const gotoInfo = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push(`/info/${this.props.loginState.user.username}`)
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }
    const gotoTourList = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push(`/wall/${this.props.loginState.user.username}/chronicle`)
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }
    const gotoQuest = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push('/quest')
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }
    const gotoKorea = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push('/region/Korea/')
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }
    const gotoJapan = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push('/region/Japan/')
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }
    const gotoChina = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push('/region/China/')
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }
    const gotoUS = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push('/region/United%20States/')
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }
    const gotoFrance = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push('/region/France/')
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }
    const gotoUK = () => {
      if (this.props.loginState.isLoggedOn) {
        this.context.router.push('/region/United%20Kingdom/')
      } else {
        alert('Login First!')
        this.context.router.push('/login')
      }
    }

    return (
      <Menu>
        <Subheader>메뉴</Subheader>
        <Divider />

        <MenuItem id="menu_wall" primaryText="My Wall" leftIcon={<RemoveRedEye />} onTouchTap={handleWallGo} />
        <MenuItem id="menu_info" primaryText="My Information" leftIcon={<PersonAdd />} onTouchTap={gotoInfo} />
        <MenuItem id="tour_list" primaryText="  Tour Chronicle" leftIcon={<ContentLink />} onTouchTap={gotoTourList} />
        <MenuItem id="quest" primaryText="Quest" leftIcon={<ContentCopy />} onTouchTap={gotoQuest} />
        <Divider />
        <Subheader>국가태그</Subheader>
        <Divider />
        <MenuItem id="korea" primaryText="#Korea" leftIcon={<img src="/img/kr.png" />} onTouchTap={gotoKorea} />
        <MenuItem id="us" primaryText="#United States" leftIcon={<img src="/img/us.png" />} onTouchTap={gotoUS} />
        <MenuItem id="france" primaryText="#France" leftIcon={<img src="/img/fr.png" />} onTouchTap={gotoFrance} />
        <MenuItem id="uk" primaryText="#United Kingdom" leftIcon={<img src="/img/uk.png" />} onTouchTap={gotoUK} />
        <MenuItem id="japan" primaryText="#Japan" leftIcon={<img src="/img/jp.png" />} onTouchTap={gotoJapan} />
        <MenuItem id="china" primaryText="#China" leftIcon={<img src="/img/ch.png" />} onTouchTap={gotoChina} />
      </Menu>
    )
  }
}

MenuBar.propTypes = {
  reverse: PropTypes.bool,
  children: PropTypes.node,
  loginState: PropTypes.object,
}

export default MenuBar
