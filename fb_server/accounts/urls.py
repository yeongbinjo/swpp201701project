from django.conf.urls import url, include
from rest_framework import routers
from accounts.views import AccountList

router = routers.DefaultRouter()
router.register(r'list', AccountList)


urlpatterns = [
    url(r'^', include(router.urls)),
]
