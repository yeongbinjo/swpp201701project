import React from 'react'
import { storiesOf } from '@kadira/storybook'
import Comments from '.'

storiesOf('Comments', module)
  .add('default', () => (
    <Comments>Hello</Comments>
  ))
  .add('reverse', () => (
    <Comments reverse>Hello</Comments>
  ))
