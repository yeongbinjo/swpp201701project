import React from 'react'
import { storiesOf } from '@kadira/storybook'
import { Posting } from 'components'

storiesOf('Posting', module)
  .add('default', () => (
    <Posting>Hello</Posting>
  ))
  .add('reverse', () => (
    <Posting reverse>Hello</Posting>
  ))
