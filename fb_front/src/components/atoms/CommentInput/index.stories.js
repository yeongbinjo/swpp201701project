import React from 'react'
import { storiesOf } from '@kadira/storybook'
import CommentInput from '.'

storiesOf('CommentInput', module)
  .add('default', () => (
    <CommentInput />
  ))
  .add('reverse', () => (
    <CommentInput reverse />
  ))
  .add('height', () => (
    <CommentInput height={100} />
  ))
  .add('invalid', () => (
    <CommentInput invalid />
  ))
  .add('type textarea', () => (
    <CommentInput type="textarea" />
  ))
  .add('type checkbox', () => (
    <CommentInput type="checkbox" />
  ))
  .add('type radio', () => (
    <CommentInput type="radio" />
  ))
  .add('type select', () => (
    <CommentInput type="select">
      <option>Option 1</option>
      <option>Option 2</option>
      <option>Option 3</option>
    </CommentInput>
  ))
