from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """담벼락 주인이 아니면 읽기만 허용하는 커스텀 권한"""

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user in [obj.author, obj.owner]:
            return True