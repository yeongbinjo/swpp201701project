import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import Radium from 'radium'
import { StyleRoot } from 'radium'
import AutoCompleteSearch from '../../molecules/AutoCompleteSearch'

const RadiumLink = Radium(Link);

const styles = {
  base: {
    zIndex: '1030',
    backgroundColor: '#1E70C0',
    border: 0,
    color: '#fff',
    cursor: 'default',
    height: '80px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    boxShadow: '0 1px 2px 0 #777',
  },
  logo: {
    fontSize: 31,
    textDecoration: 'None',
    color: 'white',
    margin: '20px',
    display: 'inline-flex',
    fontFamily: 'Arial',
    fontWeight: '700',
    flex: '0 0 auto',
  },
  logins: {
    display: 'flex',
    flex: '1 0 auto',
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: '20px',
    inputTexts: {
      display: 'inline-flex',
      margin: '10px',
    },
    inputs: {
      display: 'inline-flex',
      height: '2em',
      margin: '10px',
    },
    buttons: {
      backgroundColor: '#E28C30',
      color: 'white',
      display: 'inline-flex',
      height: '2.5em',
      padding: '5px',
      margin: '10px',
      cursor: 'pointer',
      border: '0.0625em solid transparent',
      fontSize: '0.8rem',
      fontWeight: '700',
      ':hover': {
        backgroundColor: 'white',
        color: '#E28C30',
      },
    },
  },
  linkRemove: {
    textDecoration: 'none',
    color: 'white',
    ':hover': {
      backgroundColor: 'white',
      color: '#E28C30',
    },
  },
}


class MiniLogin extends React.Component {
  render() {
    let username
    let password
    const onPushLogin = () => {
      this.props.onLogin(username.value, password.value)
    }
    const onEnter = (e) => {
      if (e.keyCode === 13) { onPushLogin() }
    }
    if (!this.props.loginState.isLoggedOn) {
      return (
        <StyleRoot style={styles.base}>
          <Link id="homeLogo" style={styles.logo} to="/"><img src={'/img/logo_sailer_noBG.png'} height="80px" alt="logo" /></Link>
          <div style={styles.logins}>
            <h4 style={styles.logins.inputTexts}>ID:</h4>
            <input style={styles.logins.inputs} id="username_mini" ref={(node) => { username = node }} />
            <h4 style={styles.logins.inputTexts}>Password:</h4>
            <input style={styles.logins.inputs} id="password_mini" type="password" onKeyDown={onEnter} ref={(node) => { password = node }} />
            <button key="loginButton_mini" id="loginButton_mini" style={styles.logins.buttons} onClick={onPushLogin}>Login</button>
            <button key="signupButton_mini" id="signupButton_mini" style={styles.logins.buttons}><RadiumLink style={styles.linkRemove} to="/SignUp">SignUp!</RadiumLink></button>
          </div>
        </StyleRoot>
      )
    }
    return (
      <StyleRoot style={styles.base}>
        <Link style={styles.logo} to="/"><img src={'/img/logo_sailer_noBG.png'} height="80px" alt="logo" /></Link>
        <AutoCompleteSearch />
        <div style={Object.assign({}, styles.logins, styles.logins.logout)}>
          <h4 style={styles.logins.welcomeTexts}>{`${this.props.loginState.user.first_name}님 반갑습니다!`}</h4>
          <button key="logoutButton_mini" id="logoutButton_mini" style={styles.logins.buttons} onClick={this.props.onLogout}>Logout</button>
        </div>
      </StyleRoot>
    )
  }
}


MiniLogin.propTypes = {
  onLogin: PropTypes.func,
  onLogout: PropTypes.func,
  loginState: PropTypes.object,
}

export default MiniLogin
