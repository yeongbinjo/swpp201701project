import React from 'react'
import PropTypes from 'prop-types'
import NewsFeed from '../../organisms/NewsFeed'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';


const styles={
  base:{
    position :'relative',
    marginTop:'0px',
    paddingTop:'0px',
    top:'0px',
    minHeight:'1000px',
  },
  img:{
    position:'relative',
    width:'100%',
  },
  title:{
    fontFamily: 'Arial',
    fontWeight: '700',
    fontSize:'50',
  },
  flag:{
    width:'200',
    height:'120',
    // float:'right',
  }
}
let serviced = [{
  name:'United Kingdom',
  language:'영어',
  capital:'런던',
  tooltip:'환상의 나라 영국으로 오세요!'},{
  name:'France',
  language:'프랑스어',
  capital:'파리',
  tooltip:'우아한 기풍의 프랑스 어떠신가요?'  }, {
  name:'Japan',
  language:'일본어',
  capital:'도쿄',
  tooltip:'소소한 전통이 살아 숨쉬는 일본!'  }, {
  name:'Korea',
  language:'한국어',
  capital:'서울',
  tooltip:'제발 한국인이면 한국부터 갑시다'  }, {
  name:'United States',
  language:'영어',
  capital:'워싱턴 D.C.',
  tooltip:'천조국의 스케일을 느껴보세요!'  },{
  name:'China',
  language:'중국어',
  capital:'베이징',
  tooltip:'대륙의 기상을 원하는 당신이라면 이 곳!'  },
]

class RegionPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
    };
  }

  handleExpandChange = (expanded) => {
    this.setState({expanded: expanded});
  };

  handleToggle = (event, toggle) => {
    this.setState({expanded: toggle});
  };

  handleExpand = () => {
    this.setState({expanded: true});
  };

  handleReduce = () => {
    this.setState({expanded: false});
  };

  render() {
    let imgUrl = '/img/'+this.props.params.region+'.jpg'
    let flagImg = '/img/flags/flag_'+this.props.params.region+'.jpg'
    console.log(imgUrl)
    console.log(flagImg)
    let subtitle, capital, language
    for(let x of serviced){
      if(x.name == this.props.params.region){
        subtitle = x.tooltip
        capital = x.capital
        language = x.language
        break
      }
    }
    return (
      <div style={styles.base}>
      <Card expanded={this.state.expanded} onExpandChange={this.handleExpandChange}>
        <CardHeader
          title={this.props.params.region}
          subtitle={subtitle}
          actAsExpander={true}
          showExpandableButton={true}
          titleStyle={styles.title}
        >

        </CardHeader>

        <CardText expandable={true}>

          <img src={flagImg} style={styles.flag} />
          <ul>
            <li>수도: {capital}</li>
            <li>언어: {language}</li>

          </ul>
        </CardText>
      </Card>
      <img src={imgUrl} style={styles.img}/>
      <NewsFeed tag={this.props.params.region} />
      </div>
    )
  }
}

RegionPage.propTypes = {
  params: PropTypes.object,
}

export default RegionPage
