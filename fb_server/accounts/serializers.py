from rest_framework import serializers
from django.contrib.auth import get_user_model
User = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    add_followees = serializers.BooleanField(required=False)

    class Meta:
        model = User
        fields = ('id', 'username', 'last_name', 'first_name', 'gender',
                  'birthday', 'password', 'followees', 'add_followees', 'profile_image', 'visited', 'quest_on','quest_done','theme_number')
        extra_kwargs = {'password': {'write_only': True}}

    # http://stackoverflow.com/questions/27586095/why-isnt-my-django-user-models-password-hashed
    def create(self, validated_data):
        user = get_user_model()(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):
        for f in UserSerializer.Meta.fields:
            if f not in validated_data:
                continue
            elif f == 'id':
                continue
            elif f == 'password':
                instance.set_password(validated_data['password'])
            elif f == 'followees':
                if validated_data.get('add_followees', True):
                    func = instance.followees.add
                else:
                    func = instance.followees.remove
                for followee in validated_data['followees']:
                    func(followee)
            else:
                setattr(instance, f, validated_data[f])
        instance.save()
        return instance

