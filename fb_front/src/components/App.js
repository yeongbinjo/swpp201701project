import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { injectGlobal } from 'styled-components'
import { updateLogin, pushLogout, pushLogin } from '../store/login/actions'
import { startChat } from '../store/userList/actions'
import MiniLogin from './molecules/MiniLogin'
import UserList from '../containers/userList/UserList'
import ChatAlertSnackbar from './organisms/ChatAlertSnackbar'
import MenuBar from './organisms/MenuBar'
import ChatDialog from './organisms/ChatDialog'
import { questListRequest } from '../store/quest/actions'

injectGlobal`
  body {
    margin: 0;
  }
`

const styles = {
  root: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100% !important',
  },

  miniLogin: {
    flex: '1 0 auto',
  },

  mainContent: {
    display: 'flex',
    flexDirection: 'row',
    maxWidth: '100%',
    minHeight: '100% !important',
  },

  menuBar: {
    flex: '0 0 240px',
    borderRight: '1px solid rgba(0,0,0,0.1)',
    borderBottom: '1px solid rgba(0,0,0,0.1)',
    minHeight: '100% !important',
  },

  content: {
    flex: '1 1 auto',
    padding: '0px',
  },

  sideBar: {
    flex: '0 0 300px',
    borderLeft: '1px solid rgba(0,0,0,0.1)',
    borderBottom: '1px solid rgba(0,0,0,0.1)',
    minHeight: '100% !important',
  },

  floatingChat: {
    position: 'fixed',
    display: 'inline-block',
    right: '300px',
    bottom: 0,
    width: '300px',
    height: '400px',
    zIndex: '1000',
  },
}

class App extends React.Component {
  render() {
    if (!this.props.loginState.isLoggedOn) {
      const loginState = JSON.parse(sessionStorage.getItem('loginState'))
      if (loginState) {
        this.props.onMount(loginState.token, loginState.user)
      }
    }
    if(this.props.questState.questGetState==0){
      this.props.onLoadQuest()
    }
    const SlideBar = () => {
      if (this.props.loginState.isLoggedOn) {
        return (
          <section style={styles.sideBar}>
            <UserList my={this.props.loginState.user} />
          </section>
        )
      }
      return (
        <section style={styles.sideBar} />
      )
    }
    const FloatingChat = () => {
      if (this.props.loginState.isLoggedOn && this.props.chatTarget) {
        return (
          <section style={styles.floatingChat}>
            <ChatDialog me={this.props.loginState.user} to={this.props.chatTarget} token={this.props.loginState.token} />
          </section>
        )
      }
      return (
        <section />
      )
    }
    const SnackBar = () => {
      if (this.props.loginState.isLoggedOn) {
        return (
          <ChatAlertSnackbar
            chatTarget={this.props.chatTarget}
            onStartChat={this.props.startChat}
            token={this.props.loginState.token}
          />
        )
      }
      return (
        <section />
      )
    }
    const MenuBarIfLoggedOn = () => {
      if (this.props.loginState.isLoggedOn) {
        return (
          <MenuBar id="menuBar" loginState={this.props.loginState} />
        )
      }
      return (
        <section />
      )
    }

    return (
      <section style={styles.root}>
        <MiniLogin id="miniLogin" loginState={this.props.loginState} onLogin={this.props.login} onLogout={this.props.logout} />
        <section style={styles.mainContent}>
          <section style={styles.menuBar}>
            <MenuBarIfLoggedOn id="MenuBarIfLoggedOn" />
          </section>
          <section style={styles.content}>
            {this.props.children}
          </section>
          <SlideBar />
          <SnackBar />
        </section>
        <FloatingChat />
      </section>
    )
  }
}

App.propTypes = {
  children: PropTypes.any,
  loginState: PropTypes.object,
  login: PropTypes.func,
  logout: PropTypes.func,
  chatTarget: PropTypes.object,
  startChat: PropTypes.func,
  onMount: PropTypes.func,
}

const mapStateToProps = (state) => {
  return {
    chatTarget: state.userList.chatTarget,
    loginState: state.login,
    questState: state.quest
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onMount: (token, account) => {
      dispatch(updateLogin(token, account))
    },
    login: (id, pw) => {
      dispatch(pushLogin(id, pw))
    },
    logout: () => {
      dispatch(pushLogout())
    },
    startChat: (to) => {
      dispatch(startChat(to))
    },
    onLoadQuest:()=>{
      dispatch(questListRequest())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
