export const postLike =( postID, char)=>{
  return{
    type: 'POST_LIKE',
    postID,
    char
  }
}

export const reviseLike =(likeID, postID, char)=>{
  return{
    type: 'REVISE_LIKE',
    likeID,
    postID,
    char
  }
}

export const deleteLike = (likeID, postID)  =>{
  return{
    type:'DELETE_LIKE',
    likeID,
    postID
  }
}

export const LikeSuccess = () =>{
  return{
    type:'LIKE_SUCCESS'
  }
}
export const afterLikeSuccess = () =>{
  return{
    type:'AFTER_LIKE_SUCCESS'
  }
}
export const likeSuccessChecker = () =>{
  return{
    type:'LIKE_SUCCESS_CHECKER'
  }
}
