const totalinitialstate = {
  comment:{
    commentState : 0, //0 IDLE, 1 WAITING, 2 SUCCESS
  },
  like:{
    likeState : 0,
  },
  login : {
    isLoggedOn: false,
    user: {
      id:0,
      username:""
    },
    token:'no token',
  },
  posting: {
    postState:0, //0: IDLE or WRITING , 1:WAITING
  },
  signUp: {
    signUpState:0, //0 when writing, 1 when wating, 2 after success
    username:''
  },
  userList : {
    userList:[],
  },
  wall: {
    wallOwner: null,
    username:'',
    getState:0, //0 IDLE // 1 profile get // 2 get failed
    wallPosts:null,
  }
}

export default totalinitialstate
