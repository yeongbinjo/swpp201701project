from django.contrib import admin

from quest.models import Quest

admin.site.register(Quest)
