// first of course react!
import React from 'react'
// require your <Map> component
import PropTypes from 'prop-types'
import {Map, MarkerGroup, PolygonGroup} from 'react-d3-map'
import {
  Annotation,
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
} from "react-simple-maps"
import { scaleLinear } from "d3-scale"
import { Tooltip, Origin, actions } from 'redux-tooltip';

var topojson = require('topojson');

const SVGOrigin = Origin.wrapBy('g');

const colorScale = scaleLinear()
  .domain([0, 1, 2]) // Max is based on China
  .range(["#BDBDBD", "#FFD54F", "#2979FF"])

var data = require('../../../../public/path/to/world-50m-with-data.json');
var data2 = topojson.feature(data,data.objects.units)
let serviced = [{
  name:'United Kingdom',
  tooltip:'환상의 나라 영국으로 오세요!'},{
  name:'France',
  tooltip:'우아한 기풍의 프랑스 어떠신가요?'  }, {
  name:'Japan',
  tooltip:'소소한 전통이 살아 숨쉬는 일본!'  }, {
  name:'Korea',
  tooltip:'제발 한국인이면 한국부터 갑시다'  }, {
  name:'United States',
  tooltip:'천조국의 스케일을 느껴보세요!'  },{
  name:'China',
  tooltip:'대륙의 기상을 원하는 당신이라면 이 곳!'  },
]


const WorldMap=({visited}, context) =>{
  console.log(context);
  const handleClick=(geography, evt) =>{
  if(geography.properties.visited>=1)
    context.router.push(`/region/${geography.properties.name}`)
}

for(let geography of data2.features){
  for(let tourObject of serviced){
    if(geography.properties.name == tourObject.name){
      geography.properties.visited = 1;
      geography.properties.tooltip = tourObject.tooltip;
      break
    }
    else{
      geography.properties.visited = 0;
      geography.properties.tooltip = geography.properties.name+"은(는) 준비중입니다!";
    }
  }
  if (visited.includes(geography.properties.name)){
    geography.properties.visited = 2;
  }
  /*
  if (visited.includes(geography.properties.name)){
    geography.properties.visited = 2;
    geography.properties.
  }
  else if(serviced.includes(geography.properties.name)){
    geography.properties.visited = 0;
  }
  else geography.properties.visited = 1;
  */
}

  return(
    <div>
      <ComposableMap style={{ width: "100%" }}>
        <ZoomableGroup>
          <Geographies disableOptimization={true}>
            {(geographies, projection) => data2.features.map((geography, i) => (
            <SVGOrigin key = {2*i} className="red" content={geography.properties.tooltip}>
              <Geography
                key={ i }
                onClick={handleClick}
                geography={ geography }
                projection={ projection }
                style={{
                  default: {
                    fill: colorScale(geography.properties.visited),
                    stroke: "black",
                    strokeWidth: 0.1,
                    outline: "none",
                  },
                }}
              />
          </SVGOrigin>
            ))}
          </Geographies>
        </ZoomableGroup>
      </ComposableMap>
      <Tooltip />
    </div>
  )
}
WorldMap.contextTypes = {
  router: PropTypes.object,
}
export default WorldMap
