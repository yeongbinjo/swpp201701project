import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getNewsFeeds } from '../../../store/newsFeed/actions'
import Post from '../../atoms/Post/index'

const style = {
  newsFeedPost: {
    margin: '16px',
  },
  noPostWrapper: {
    display: 'flex',
    height: '100%',
    minHeight: '100% !important',
    margin: 'auto',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  noPostMessage: {
    margin: '8px 0',
  },
}


class NewsFeed extends React.Component {

  render() {
    console.log(this.props.newsFeedState.tag);
    if(this.props.newsFeedState.isDefault || this.props.newsFeedState.tag!=this.props.tag){
      this.props.getNewsFeed(this.props.tag)
    }
    if (this.props.newsFeedState.newsFeeds.length !== 0) {
      return (
        <section>
          {this.props.newsFeedState.newsFeeds.map(newsFeed =>
            <div key={newsFeed.id} style={style.newsFeedPost} >
              <Post post={newsFeed} />
            </div>
        )}
        </section>
      )
    } return (
      <section style={style.noPostWrapper}>
        <img src="/img/main.png" height="300px" alt="보여줄 포스트가 없어요!" />
        <div style={style.noPostMessage}>당신의 이야기를 공유해보세요!</div>
      </section>
    )
  }
}

NewsFeed.propTypes = {
  getNewsFeed: PropTypes.func,
  newsFeeds: PropTypes.array,
  tag: PropTypes.string,
}

NewsFeed.defaultProps = {
  tag: '',
}

const mapStateToProps = (state) => {
  return {
    newsFeedState: state.newsFeed,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getNewsFeed: (tag) => {
      dispatch(getNewsFeeds(tag))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsFeed)
