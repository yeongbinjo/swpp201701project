import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { tempChange, pushImageChange } from '../../../store/infoChange/actions'
import Dropzone from 'react-dropzone'
import { Link } from 'react-router'
import styled, { css } from 'styled-components'
import { font, palette } from 'styled-theme'
import { ifProp } from 'styled-tools'

const styles = {
  previewComponent:{
    marginLeft: "50px",
  },
  fileInput:{
    borderBottom: "4px solid lightgray",
    borderRight: "4px solid lightgray",
    borderTop: "1px solid black",
    borderLeft: "1px solid black",
    padding: "10px",
    margin: "15px",
    cursor: "pointer",
  },
  imgPreview:{
    textAlign: "center",
    margin: "5px 15px",
    width: "200px",
    height: "200px",
    borderLeft: "1px solid gray",
    borderRight: "1px solid gray",
    borderTop: "5px solid gray",
    borderBottom: "5px solid gray",
  },
  img:{
      width:"auto",
      height:"auto",
      maxWidth:"200px",
      maxHeight:"200px",
      // verticalAlign:"middle",
  }
};


export class ImageUpload extends React.Component {
  state = {
    // file: null,
    file: '',imagePreviewUrl: '',
  };

  handleSubmit() {
    this.props.onChange(this.props.user, this.props.infoChangeState.image)
  }

  render() {
    const handleImageChange = (files) => {
      this.props.onTempChange(files[0], files[0].preview)
    }

    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img style={styles.img} src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText">미리보기</div>);
    }

    return (
      <div style={styles.previewComponent}>
        <Dropzone
        onDrop={handleImageChange}
        accept="image/jpeg, image/png"
        >
        이미지 업로드
        </Dropzone>
        <div style={styles.imgPreview}>
          {$imagePreview}
        </div>
        <button className="submitButton" type="submit" onClick={(e)=>this.handleSubmit(e)}>Upload Image</button>
      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    infoChangeState: state.infoChange
  }
}

const mapDispatchToProps= (dispatch)=>{
  return{
    onChange: (user, image)=>{
      dispatch(pushImageChange(user, image))
    },
    onTempChange: (image, imagePreviewUrl)=>{
      dispatch(tempChange(image, imagePreviewUrl))
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ImageUpload)