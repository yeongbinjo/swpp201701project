import React from 'react'
import SignUpForm from '../../molecules/SignUpForm'

const formAlign={
  margin: '0',
  position: 'relative',
  paddingLeft:'40%',
  paddingRight:'30%',
  paddingTop:'5%',
  paddingBottom:'25%'
}

const SignUpPage = () => {
  return (

    <div style={formAlign}>
      <SignUpForm/>
    </div>
  )
}

export default SignUpPage
