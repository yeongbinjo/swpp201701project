import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Dropzone from 'react-dropzone'
import RaisedButton from 'material-ui/RaisedButton'
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import { textType, imageChange, pushPost } from '../../../store/posting/actions'

const boxStyle = {
  border: '1px solid rgba(0,0,0,0.1)',
  borderRadius: '5px',
}
const textareaStyle = {
  display: 'block',
  width: '100%',
  margin: '0',
  boxSizing: 'border-box',
  padding: '5px',
  height: '180px',
  border: 'none',
  overflow: 'auto',
  outline: 'none',
  resize: 'none',
  fontSize: '14px',
}
const buttonStyle = {
  display: 'relative',
  left: '0px',
  height: '50px',
  borderTopLeftRadius: 0,
  borderTopRightRadius: 0,
  labelStyle: {
    position: 'relative',
    top: '15px',
    fontSize: '15px',
    fontWeight: '600',
  },
}
const imagePreviewStyle = {
  position: 'relative',
  display: 'flex',
  float: 'left',
  height: '50px',
}
const dropzoneStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  margin: '16px',
  height: '70px',
  border: 'dashed 4px rgb(30, 112, 192)',
  borderImage: 'url("https://i.stack.imgur.com/wLdVc.png") 2 round',
  borderImageSource: 'url("https://i.stack.imgur.com/LKclP.png")',
  borderImageSlice: 2,
  borderImageRepeat: 'round',
}
const tagInputStyle = {
  margin: '16px',
}

const ImagePreview = ({ imagePreviewURL }) => {
  if (imagePreviewURL) {
    return (
      <img style={imagePreviewStyle} src={imagePreviewURL} alt="미리보기" />
    )
  }
  return null
}

ImagePreview.propTypes = {
  imagePreviewURL: PropTypes.string,
}

class Posting extends React.Component {
  constructor(props) {
    super(props)

    this.onTagsChange = this.onTagsChange.bind(this)

    this.state = {
      tags: [],
    }
  }

  onTagsChange(tags) {
    this.setState({ tags })
  }

  render() {
    if (!this.props.loginState.isLoggedOn) {
      return (
        <div style={boxStyle}>
          <h1>Log in if you want to POST</h1>
        </div>
      )
    }

    const onSubmit = () => {
      if (this.props.statefunction.postState === 0) {
        this.props.onPush(this.props.statefunction.text, this.props.ownerID, this.state.tags)
      }
      this.setState({ tags: [] })
    }
    if (this.props.statefunction.postState === 2) {
      document.getElementById('textBox').value = ''
    }
    const placeholderSet = this.props.loginState.user.id === this.props.ownerID ?
      `${this.props.loginState.user.first_name}님, 오늘은 또 어떤 곳을 여행하셨나요?` :
      `${this.props.ownerFirstName}님에게 글을 남겨보세요!`

    const handleTextChange = (e) => {
      this.props.onType(e.target.value)
    }
    const onDrop = (files) => {
      this.props.onImageChange(files[0], files[0].preview)
    }

    return (
      <div style={boxStyle}>
        <textarea
          style={textareaStyle}
          id="textBox"
          onChange={handleTextChange}
          value={this.props.statefunction.text}
          placeholder={placeholderSet}
        />
        <div style={tagInputStyle}>
          <TagsInput
            style={{ border: 'none' }}
            value={this.state.tags}
            onChange={this.onTagsChange}
          />
        </div>
        <ImagePreview imagePreviewURL={this.props.statefunction.imagePreviewURL} />
        <Dropzone
          onDrop={onDrop}
          accept="image/jpeg, image/png"
          style={dropzoneStyle}
        >
          이미지를 드래그해주세요
        </Dropzone>
        <RaisedButton
          id="postButton"
          style={buttonStyle}
          primary
          onTouchTap={onSubmit}
          labelStyle={buttonStyle.labelStyle}
          label="올 리 기"
        />
      </div>
    )
  }
}

Posting.propTypes = {
  statefunction: PropTypes.object,
  onImageChange: PropTypes.func,
  onType: PropTypes.func,
  onPush: PropTypes.func,
  ownerFirstName: PropTypes.string,
  ownerID: PropTypes.number,
  loginState: PropTypes.object,
}

const mapStateToProps = (state) => {
  return {
    statefunction: state.posting,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onType: (text) => {
      dispatch(textType(text))
    },
    onImageChange: (image, imagePreviewURL) => {
      dispatch(imageChange(image, imagePreviewURL))
    },
    onPush: (text, ownerID, tags) => {
      dispatch(pushPost(text, ownerID, tags))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Posting)
