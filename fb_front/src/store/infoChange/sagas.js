import { call, put, fork, take, cancel, select } from 'redux-saga/effects'
import api from 'services/api'
import * as actions from './actions'
import { UPDATE_LOGIN, UPDATE_USERINFO } from '../login/actions'
import { backendUrl } from 'config'
import { browserHistory } from 'react-router';
import { getID, getToken, getQuestDone } from '../login/selectors'
import { getWallOwnerName } from '../wall/selectors'

const url = backendUrl

export function* imageChange() {
  while(true){
    const action = yield take('PROFILE_IMAGE_CHANGE')
    const formData = new FormData()
    formData.append("profile_image",action.image);

    const token = JSON.parse(sessionStorage.getItem('loginState')).token
    const response = yield call(fetch, `${backendUrl}/list/${action.user.id}/`, {
      method: 'PATCH',
      mode: 'cors',
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        Authorization: `Token ${token}`,
      },
      body: formData,
    })

    if (response.status==200) {
      alert('프로필 사진이 변경되었습니다')
      yield put({ type:'SUCCESS' })
      // console.log("[Before]", action.user.profile_image)
      const user = yield call(api.get, `${url}/list/${action.user.id}/`)
      // console.log("[After]", action.user.profile_image)
      yield put({ type: UPDATE_USERINFO, user: user })
    }
    else alert('에러 발생')
  }
}

export function* themeChange(theme){
  const userid = yield select(getID)
  const token = yield select(getToken)
  const response = yield call(api.patch, `${backendUrl}/list/${userid}/`, {theme_number:theme
  },{
     method: 'PATCH',
     mode:'cors',
     headers: {
       Accept: 'application/json, application/xml, text/plain, text/html, *.*',
       'Content-Type': 'application/json',
       Authorization: `Token ${token}`,
     },
   })
   if(response){
     yield put({type:'UPDATE_LOGIN',token:token, user:response})
     const wallOwnerName = yield select(getWallOwnerName)
     if(wallOwnerName!='') yield put({type:'OWNER_SET',wallOwnerName:wallOwnerName})
   }
}
export function* themeChangeFlow(){
  while(true){
    const {theme}= yield take('THEME_CHANGE_REQUEST')
    yield call(themeChange, theme)
  }
}


export default function* (){
  yield fork(imageChange)
  yield fork(themeChangeFlow)
}
