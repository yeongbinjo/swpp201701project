import { initialstate } from './selectors'


export const commentUpdate=(wallPosts,comments,id)=>{
  for(let content of wallPosts){
    if(content.id == id){
      content.comments=comments
      return wallPosts
    }
  }
}

export const likeUpdate=(wallPosts,likes,id)=>{
  for(let content of wallPosts){
    if(content.id == id){
      content.likes=likes
      return wallPosts
    }
  }
}

const wallReducer = (state = initialstate, action) => {
  switch (action.type) {
    case 'OWNER_SET':
      return{
        ...state,
        getState:1,
        username:action.wallOwnerName,
      }
    case 'SET_SUCCESS':
      return{
        ...state,
        getState:2,
        wallOwner:action.wallOwner,
        wallPosts: action.wallPosts
      }
    case 'SET_FAILED':
      return{
        ...state,
        getState:3,
      }
    case 'PARTIAL_POST_UPDATE':
      return{
        ...state,
        getState:2,
        wallPosts: action.wallPosts
      }
    case 'PARTIAL_COMMENT_UPDATE':
      return {
        ...state,
        getState:2,
        wallPosts: commentUpdate(state.wallPosts,action.comments, action.postID)
      }
      case 'PARTIAL_LIKE_UPDATE':
      return{
        ...state,
        getState:2,
        wallPosts: likeUpdate(state.wallPosts,action.likes, action.postID)
      }
    default:
      return state
  }
}

export default wallReducer
