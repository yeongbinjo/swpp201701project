export const TOGGLE_LIKE = 'TOGGLE_LIKE';

export const toggleLike = (post, userId) => {
  return {
    type: TOGGLE_LIKE,
    post,
    userId,
  }
}

