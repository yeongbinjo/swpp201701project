import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { yellow100 } from 'material-ui/styles/colors'
import CircularProgress from 'material-ui/CircularProgress'
import { updateLogin } from '../../../store/login/actions'
import { ownerSet } from '../../../store/wall/actions'


export class WallPage extends React.Component {
  render() {
    // initial wall setting
    if (this.props.wallState.getState === 0) {
      this.props.ownerSet(this.props.params.wallOwner)
    }

    // single-page wallOwner change
    if (this.props.wallState.getState === 2 && this.props.wallState.wallOwner.username !== this.props.params.wallOwner) {
      this.props.ownerSet(this.props.params.wallOwner)
    }

    if (this.props.wallState.getState <= 1) {
      return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100%' }}>
          <CircularProgress />
        </div>
      )
    } else if (this.props.wallState.getStatere === 2) {
      return (<div><h2>해당 사용자를 찾을 수 없습니다.</h2></div>)
    }
    return (<div>{this.props.children}</div>)
  }
}

WallPage.propTypes = {
  reverse: PropTypes.bool,
  wallState: PropTypes.object,
  loginState: PropTypes.object,
  params: PropTypes.object,
  ownerSet: PropTypes.func,
  children: PropTypes.object,
}

const mapStateToProps = (state) => {
  return {
    wallState: state.wall,
    loginState: state.login,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginSet: (token, account) => {
      dispatch(updateLogin(token, account))
    },
    ownerSet: (wallOwnerName) => {
      dispatch(ownerSet(wallOwnerName))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WallPage)
