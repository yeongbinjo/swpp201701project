import loginReducer from './reducer'
import { initialstate, getID, getToken, getLoginState } from './selectors'
import totalinitialstate from '../totalinitialstate'

describe('login Reducer', () => {
  it('has a default state', () => {
    expect(loginReducer(undefined, { type: 'unexpected' })).toEqual(
      initialstate
    )
  })
  // 테스트 환경에서 ReferenceError: sessionStorage is not defined 가 발생하여 일단 update login 테스트를 수행할 수 없습니다
  // 추후 sessionStorage의 mock을 사용해서 테스트 해야 할 듯

  it('update login', () => {
    expect(loginReducer(undefined, { type: 'UPDATE_LOGIN', token: 'abcd', user: { id: 0, visited: JSON.stringify(['France', 'Russia']) } })).toEqual({
      ...initialstate,
      isLoggedOn: true,
      token: 'abcd',
      user: {
        id: 0,
        visited: JSON.parse(JSON.stringify(['France', 'Russia'])),
      },
    })
  })

  it('push logout', () => {
    expect(loginReducer(undefined, { type: 'PUSH_LOGOUT' })).toEqual(
      initialstate
    )
  })

  it('getID', () => {
    expect(getID(totalinitialstate)).toEqual(0)
  })
  it('getToken', () => {
    expect(getToken(totalinitialstate)).toEqual('no token')
  })
  it('getLoginState', () => {
    expect(getLoginState(totalinitialstate)).toEqual(false)
  })
})
