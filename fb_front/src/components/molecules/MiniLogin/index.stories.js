import React from 'react'
import { storiesOf } from '@kadira/storybook'
import { MiniLogin } from 'components'

storiesOf('MiniLogin', module)
  .add('default', () => (
    <MiniLogin>Hello</MiniLogin>
  ))
  .add('reverse', () => (
    <MiniLogin reverse>Hello</MiniLogin>
  ))
