import React from 'react'
import { storiesOf } from '@kadira/storybook'
import { Post } from 'components'

storiesOf('Post', module)
  .add('default', () => (
    <Post>Hello</Post>
  ))
  .add('reverse', () => (
    <Post reverse>Hello</Post>
  ))
