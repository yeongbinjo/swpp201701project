import React from 'react'
import { Router, Route, IndexRoute, hashHistory, browserHistory } from 'react-router'

import App from './components/App'
import SignUpPage from './components/pages/SignUpPage'
import LoginPage from './components/pages/LoginPage'
import Wall from './components/organisms/Wall'
import MyInfo from './components/molecules/MyInfo'
import HomePage from './components/pages/HomePage'
import TagPage from './components/pages/TagPage'
import RegionPage from './components/pages/RegionPage'
import Chronicle from './components/organisms/Chronicle'
import Quest from './components/organisms/Quest'
import WallPage from './components/pages/WallPage'

const routes = (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={HomePage} />
      <Route path="/wall/:wallOwner" component={WallPage}>
        <IndexRoute component={Wall} />
        <Route path="/wall/:wallOwner/chronicle" component={Chronicle} />
      </Route>
      <Route path="/quest" component={Quest} />
      <Route path="/info/:wallOwner" component={MyInfo} />
      <Route path="/signup" component={SignUpPage} />
      <Route path="/login" component={LoginPage} />
      <Route path="/tag/:tag" component={TagPage} />
      <Route path="/region/:region" component={RegionPage} />
    </Route>
  </Router>

)

export default routes
