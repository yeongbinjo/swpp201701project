import { call, put, fork, take, cancel, select } from 'redux-saga/effects'
import api from 'services/api'
import * as actions from './actions'
import { delay } from 'redux-saga'
import { backendUrl } from 'config'
import { browserHistory } from 'react-router'

const url = backendUrl

export function* login(id, pw) {
  try {
    const token = yield call(api.post, `${url}/api-token-auth/`, { username: id, password: pw })
    const user = yield call(api.get, `${url}/list/username/${id}/`)

/*
    프로필 사진 구현을 위해 추가
    ${url}/list/username/${id}/ 에서의 profile_image와
    ${url}/list/${user.id}/ 에서의 profile_image의 경로가 다른데 이유를 알 수 없어서 어쩔 수 없이 이렇게 함
*/
    const user2 = yield call(api.get, `${url}/list/${user.id}/`)
    user.profile_image = user2.profile_image

    yield put({ type: 'UPDATE_LOGIN', token: token.token, user })
  } catch (e) {
    console.log(e)
    alert('invalid ID/PW')
  }
}

export function* loginFlow() {
  while (true) {
    const action = yield take(['PUSH_LOGIN', 'PUSH_LOGOUT'])
    if (action.type === 'PUSH_LOGIN') {
      yield fork(login, action.id, action.pw)
    } else if (action.type === 'PUSH_LOGOUT') {
      sessionStorage.removeItem('loginState')
    }
  }
}

export default function* () {
  yield fork(loginFlow)
}
