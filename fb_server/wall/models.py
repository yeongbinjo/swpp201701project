# coding: utf-8
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from webpush.models import Subscription
from pywebpush import WebPusher, webpush, WebPushException


def upload_to(instance, filename):
    return 'post_images/{}/{}'.format(instance.owner.id, filename)


class Tag(models.Model):
    name = models.TextField(unique=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="owner")
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="author", null=True)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    image = models.ImageField(null=True, blank=True, upload_to=upload_to)
    tags = models.ManyToManyField(Tag, blank=True, related_name='tags')
    location = models.TextField(default='')


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="comments")
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="comment")
    content = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    is_anonymous = models.BooleanField(default=False)


class Like(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    like = models.CharField(choices=(
        ('좋아요', 'L'),
        ('최고에요', 'W'),
        ('화나요', 'A'),
        ('슬퍼요', 'S'),
        ('웃겨요', 'F'),
        ('멋져요', 'C')
    ), max_length=1)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='likes')

    class Meta:
        unique_together = ['user', 'post']


@receiver(post_save, sender=Comment)
def comment_created_handler(sender, instance=None, created=False, **kwargs):
    if created:
        for subscription in Subscription.objects.filter(Q(user=instance.post.author) | Q(user=instance.post.owner)):
            data = '%s님이 게시글에 댓글을 등록하였습니다.' % instance.author
            try:
                webpush(subscription.to_dict(), data,
                        vapid_private_key=settings.WEBPUSH_PRIVATE_KEY,
                        vapid_claims={
                            "sub": "mailto:whoknowwhat0623@gmail.com",
                        })
            except WebPushException:
                pass


@receiver(post_save, sender=Post)
def post_created_handler(sender, instance=None, created=False, **kwargs):
    if created:
        followers = instance.owner.user_followees.all() | instance.author.user_followees.all()
        for subscription in Subscription.objects.filter(user__in=followers):
            data = '%s님의 새로운 포스트가 등록되었습니다.' % instance.author
            try:
                webpush(subscription.to_dict(), data,
                        vapid_private_key=settings.WEBPUSH_PRIVATE_KEY,
                        vapid_claims={
                            "sub": "mailto:whoknowwhat0623@gmail.com",
                        })
            except WebPushException:
                pass
