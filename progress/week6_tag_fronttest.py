# please use after setting environmet variable,(BACKEND_URL = http://54.187.174.208:8000)
import time
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# browser = webdriver.Chrome()
browser = webdriver.Chrome('/usr/local/bin/chromedriver')
url = '{0}:{1}'.format(os.environ.get('HOST', 'http://54.187.174.208'), os.environ.get('PORT', 3000))

# home page
browser.get(url + '/')
time.sleep(1)
print("Logging in as USER1...")
# fill in inputboxes
userID = 'USER1'
time.sleep(2)
password = 'user1111'
time.sleep(2)
username_mini = browser.find_element_by_id('username_mini')
username_mini.send_keys(userID)
time.sleep(1)

password_mini =browser.find_element_by_id('password_mini')
password_mini.send_keys(password)
time.sleep(1)

loginButton = browser.find_element_by_id('loginButton_mini')
loginButton.click()
time.sleep(2)
print("Successfully logged in!")
print("Make some tags...")
time.sleep(2)
textBox = browser.find_element_by_class_name('react-tagsinput-input')
# textBox.click()
# textBox.send_keys(Keys.RETURN)
textBox.send_keys("firsttag"+Keys.RETURN)
print("made the firsttag")
time.sleep(3)
textBox = browser.find_element_by_class_name('react-tagsinput-input')
textBox.send_keys("secondtag"+Keys.RETURN)
print("made the secondtag")
time.sleep(3)
textBox = browser.find_element_by_class_name('react-tagsinput-input')
textBox.send_keys("thirdtag"+Keys.RETURN)
print("made the thirdtag")
time.sleep(3)
print("Now remove two tags except the last one....")
time.sleep(3)
removeButton = browser.find_element_by_class_name('react-tagsinput-remove')
removeButton.click()
time.sleep(3)
removeButton = browser.find_element_by_class_name('react-tagsinput-remove')
removeButton.click()
time.sleep(3)
print("Click the tag named thirdtag to show posts that have the tag")
time.sleep(3)
tagclick = browser.find_element_by_class_name('react-tagsinput-tag').find_element_by_tag_name('span')
tagclick.click()
time.sleep(3)


#logout
logoutButton = browser.find_element_by_id('logoutButton_mini')
logoutButton.click()
time.sleep(1)

browser.quit()
print("Successfully finished!")