import json
from urllib.parse import parse_qs, urlparse

from channels import Group
from channels.generic.websockets import WebsocketConsumer
from channels.handler import AsgiRequest
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.utils.timezone import now
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.settings import api_settings

from chat.auth_token import RestTokenConsumerMixin
from chat.models import Message

User = get_user_model()
authenticators = [auth() for auth in api_settings.DEFAULT_AUTHENTICATION_CLASSES]


class ChatServer(RestTokenConsumerMixin, WebsocketConsumer):
    http_user = True
    rest_user = True

    strict_ordering = False
    slight_ordering = False

    def __init__(self, message, **kwargs):
        super(ChatServer, self).__init__(message, **kwargs)

    def connect(self, message, **kwargs):
        query_dict = self._get_query_dict_from_message(message)
        try:
            user = self._get_user_from_message_and_token(message, query_dict['token'][0])
        except KeyError:
            self.message.reply_channel.send({"close": True})
            return

        if not user.is_authenticated:
            self.message.reply_channel.send({"close": True})
            return

        try:
            to = int(query_dict['to'][0])
            User.objects.get(pk=to)
        except (KeyError, ValueError):
            # /ws/chat/ 으로 접속
            Group('user-%d' % user.pk).add(self.message.reply_channel)
            self.message.channel_session['user_id'] = user.pk
            self.message.reply_channel.send({"accept": True})
            return
        except User.DoesNotExist:
            # /ws/chat/?to= 로 접속했으나 상대 유저가 존재하지 않음
            self.message.reply_channel.send({"close": True})
            return

        # /ws/chat/?to= 로 접속
        self.message.reply_channel.send({"accept": True})
        group_name = 'message-%d-%d' % (min(user.pk, to), max(user.pk, to))
        self.message.channel_session['group_name'] = group_name
        self.message.channel_session['to'] = to
        self.message.channel_session['token'] = query_dict['token'][0]
        Group(group_name).add(self.message.reply_channel)
        self._send_previous_messages(group_name)

    def receive(self, text=None, bytes=None, **kwargs):
        if 'group_name' not in self.message.channel_session:
            return
        user = self._get_user_from_message_and_token(self.message, self.message.channel_session['token'])

        data = json.loads(text)
        group_name = self.message.channel_session['group_name']
        self._send_message(group_name, {
            'id': user.pk,
            'name': str(user),
            'content': data['message'],
            'created': str(now())
        })
        self._save_message(group_name, user, data['message'])
        self._send_message('user-%d' % self.message.channel_session['to'], {
            'id': user.pk,
            'first_name': user.first_name,
            'last_name': user.last_name
        })

    def disconnect(self, message, **kwargs):
        if 'group_name' in self.message.channel_session:
            Group(self.message.channel_session['group_name']).discard(self.message.reply_channel)
        else:
            Group('user-%d' % self.message.channel_session['user_id']).discard(self.message.reply_channel)

    @staticmethod
    def _send_message(group_name, message):
        Group(group_name).send({
            'text': json.dumps(message)
        })

    @staticmethod
    def _save_message(channel, user, message_content):
        Message.objects.create(
            channel=channel,
            user=user,
            content=message_content
        )

    def _send_previous_messages(self, channel):
        for message in Message.objects.filter(channel=channel).order_by('-created')[:10:-1]:
            self._send_message(channel, message.to_dict())

    # from https://gist.github.com/leonardoo/9574251b3c7eefccd84fc38905110ce4
    @staticmethod
    def _get_user_from_message_and_token(message, token):
        try:
            # We want to parse the WebSocket (or similar HTTP-lite) message
            # to get cookies and GET, but we need to add in a few things that
            # might not have been there.
            if "method" not in message.content:
                message.content['method'] = "FAKE"
            request = AsgiRequest(message)

        except Exception as e:
            raise ValueError("Cannot parse HTTP message - are you sure this is a HTTP consumer? %s" % e)

        # Make sure there's a session key
        if token:
            auth_token = token
        else:
            auth_token = request.GET.get("token", None)

        if auth_token:
            # comptatibility with rest framework
            request._request = {}
            request.META["HTTP_AUTHORIZATION"] = "token {}".format(auth_token)
            for authenticator in authenticators:
                try:
                    return authenticator.authenticate(request)[0]
                except AuthenticationFailed:
                    pass
        return AnonymousUser()

    @staticmethod
    def _get_query_dict_from_message(message):
        if 'query_string' not in message.content:
            query_string = urlparse(message.content['path']).query
        else:
            query_string = message.content['query_string']
        return parse_qs(query_string)
