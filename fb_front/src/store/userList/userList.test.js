import userListReducer from './reducer'
import * as actions from './actions'
import { initialstate } from './selectors'
import totalinitialstate from '../totalinitialstate'

describe('userList Reducer', () => {
  it('has a default state', () => {
    expect(userListReducer(undefined, { type: 'unexpected' })).toEqual(
      initialstate
    )
  })
  it('set user list', () => {
    expect(userListReducer(undefined, { type: 'SET_USER_LIST', userList: 'hello' })).toEqual({
      ...initialstate,
      userList: 'hello',
    })
  })
  it('start chat', () => {
    expect(userListReducer(undefined, { type: 'START_CHAT', user: 'hello' })).toEqual({
      ...initialstate,
      chatTarget: 'hello',
    })
  })
  it('close chat', () => {
    expect(userListReducer(undefined, { type: 'CLOSE_CHAT' })).toEqual({
      ...initialstate,
      chatTarget: null,
    })
  })
})
