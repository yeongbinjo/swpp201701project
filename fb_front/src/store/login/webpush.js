import api from 'services/api'
import { backendUrl } from 'config'

const applicationServerPublicKey = 'BPFqrQSfCj93ACQB_p10e_9BcwSvVWkOL5q1GCM4NYko0cHfOLDGCmz-s792-oMqaHSWRjSEyuo_VvuPGeiVeyw'

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4)
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/')

  const rawData = window.atob(base64)
  const outputArray = new Uint8Array(rawData.length)

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i)
  }
  return outputArray
}

function updateSubscriptionOnServer(subscription) {
  const token = JSON.parse(sessionStorage.getItem('loginState')).token
  const subscriptionJson = JSON.parse(JSON.stringify(subscription))
  api.post(`${backendUrl}/subscriptions/`,
    { endpoint: subscription.endpoint,
      p256dh: subscriptionJson.keys.p256dh,
      auth: subscriptionJson.keys.auth },
    { method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        'Content-Type': 'application/json',
        Authorization: `Token ${token}` } })
}

export function subscribeUser() {
  const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey)

  if ('serviceWorker' in navigator && 'PushManager' in window) {
    console.log('Service Worker and Push is supported')

    navigator.serviceWorker.register('sw.js')
      .then((swReg) => {
        console.log('Service Worker is registered', swReg)

        swReg.pushManager.getSubscription()
          .then((subscription) => {
            const isSubscribed = !(subscription === null)

            if (isSubscribed) {
              console.log('User IS subscribed.')
              updateSubscriptionOnServer(subscription)
            } else {
              console.log('User is NOT subscribed.')
              swReg.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey,
              })
                .then((subscription) => {
                  console.log('User is subscribed:', subscription)

                  updateSubscriptionOnServer(subscription)
                })
                .catch((err) => {
                  console.log('Failed to subscribe the user: ', err)
                })
            }
          })
      })
      .catch((error) => {
        console.error('Service Worker Error', error)
      })
  } else {
    console.warn('Push messaging is not supported')
  }
}
