import { initialstate } from './selectors'
import { FULLNAME_LIST, CLOSE_CHAT, SET_USER_LIST, START_CHAT } from './actions'

const userListReducer = (state = initialstate, action) => {
  switch (action.type) {
    case FULLNAME_LIST:
      return {
        ...state,
        userList: action.userList,
      }
    case SET_USER_LIST:
      return {
        ...state,
        userList: action.userList,
      }
    case START_CHAT:
      return {
        ...state,
        chatTarget: action.user,
      }
    case CLOSE_CHAT:
      return {
        ...state,
        chatTarget: null,
      }
    default:
      return state
  }
}

export default userListReducer
