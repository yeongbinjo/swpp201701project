import signUpReducer, {commentUpdate, likeUpdate} from './reducer'
import * as actions from './actions'
import { initialstate} from './selectors'
import totalinitialstate from '../totalinitialstate'

describe('signUp Reducer',() =>{

  it('has a default state',() =>{
    expect(signUpReducer(undefined,{type:'unexpected'})).toEqual(
      initialstate
    )
  })
  it('owner setting',() =>{
    expect(signUpReducer(undefined,{type:'OWNER_SET', wallOwnerName:'hello'})).toEqual({
      ...initialstate,
      username:'hello',
      getState:1,
    })
  })
  it('setiing success',() =>{
    expect(signUpReducer(undefined,{type:'SET_SUCCESS', wallOwner:'object', wallPosts:[]})).toEqual({
      ...initialstate,
      getState:2,
      wallOwner:'object',
      wallPosts: []
    })
  })
  it("setting failed",() =>{
    expect(signUpReducer(undefined,{type:'SET_FAILED'})).toEqual({
      ...initialstate,
      getState:3,
    })
  })
  it('partial post request',() =>{
    expect(signUpReducer(undefined,{type:'PARTIAL_POST_UPDATE', wallPosts:[]})).toEqual({
      ...initialstate,
      getState:2,
      wallPosts: []
    })
  })
  it('partial comment update',() =>{
    expect(signUpReducer({...initialstate, wallPosts:[{id:1},{id:2}]},{type:'PARTIAL_COMMENT_UPDATE', comments:[{id:3},{id:4}], postID:2 })).toEqual({
      ...initialstate,
      getState:2,
      wallPosts: commentUpdate([{id:1},{id:2}], [{id:3},{id:4}], 2)
    })
  })
  it('partioal like update',() =>{
    expect(signUpReducer({...initialstate, wallPosts:[{id:1},{id:2}]},{type:'PARTIAL_LIKE_UPDATE', likes:[{id:3},{id:4}], postID:2 })).toEqual({
      ...initialstate,
      getState:2,
      wallPosts: likeUpdate([{id:1},{id:2}], [{id:3},{id:4}], 2)
    })
  })

})
