export const pushVerification = (id, pw) =>{
  return{
    type:'PUSH_VERIFICATION',
    id,
    pw
  }
}

export const success = () =>{
  return{
    type:'SUCCESS',
  }
}

export const reset = () =>{
  return{
    type:'RESET'
  }
}

export const change = (user, pw) =>{
  return{
    type:'CHANGE',
    user,
    pw
  }
}