export const postComment = (index, text) => {
  return {
    type: 'POST_COMMENT',
    index,
    text,
  }
}
export const postCommentSuccess = () => {
  return {
    type: 'POST_COMMENT_SUCCESS',
  }
}
export const afterCommentSuccess = () => {
  return {
    type: 'AFTER_COMMENT_SUCCESS',
  }
}
export const commentSuccessChecker = () => {
  return {
    type: 'COMMENT_SUCCESS_CHECKER',
  }
}
