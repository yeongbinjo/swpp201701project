export const UPDATE_LOGIN = 'UPDATE_LOGIN'
export const UPDATE_USERINFO = 'UPDATE_USERINFO'

export const pushLogin = (id, pw) =>{
  return{
    type:'PUSH_LOGIN',
    id,
    pw
  }
}

export const updateLogin = (token, user) =>{
  return{
    type:'UPDATE_LOGIN',
    token,
    user
  }
}

export const loginFailed = () =>{
  return{
    type:'LOGIN_FAILED'
  }
}

export const pushLogout = () =>{
  return{
    type:'PUSH_LOGOUT'
  }
}

export const updateUserInfo = (user) =>{
  return{
    type:'UPDATE_USERINFO',
    user
  }
}