export const initialstate = {
  postState:0, //0: IDLE or WRITING , 1:WAITING
  text:'',
  image:null,
  imagePreviewURL:null,
}
export const getPostState = state => state.posting.postState;
export const getImage = state => state.posting.image;
