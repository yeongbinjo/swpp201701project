from django.contrib import admin
from django.contrib.auth import get_user_model

# Needed to manage users in admin page
admin.site.register(get_user_model())
