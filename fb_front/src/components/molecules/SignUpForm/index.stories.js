import React from 'react'
import { storiesOf } from '@kadira/storybook'
import { SignUpForm } from 'components'

storiesOf('SignUpForm', module)
  .add('default', () => (
    <SignUpForm>Hello</SignUpForm>
  ))
  .add('reverse', () => (
    <SignUpForm reverse>Hello</SignUpForm>
  ))
