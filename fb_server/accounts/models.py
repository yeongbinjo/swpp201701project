from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import AbstractUser
from quest.models import Quest


# triggered when new user is created,
# create token and save it to the DB
def upload_to(instance, filename):
    return 'profile_images/{}/{}'.format(instance.username, filename)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class User(AbstractUser):
    gender = models.IntegerField(default=1)  # '1'isMan '2'isWoman
    birthday = models.DateField(default=now)
    followees = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name="user_followees")
    profile_image = models.ImageField(null=True, blank=True, upload_to=upload_to)
    visited = models.TextField(default="[]")
    quest_on = models.ForeignKey(Quest, null=True, blank=True, related_name="quest_chooser")
    quest_done = models.ManyToManyField(Quest, blank=True, related_name="quest_finisher")
    theme_number = models.IntegerField(default=0)

    def __str__(self):
        return self.username
