import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { grey100, grey400, red500 } from 'material-ui/styles/colors'
import IconButton from 'material-ui/IconButton'
import ActionFavorite from 'material-ui/svg-icons/action/favorite'
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border'
import { toggleLike } from '../../../store/likeToggleButton/actions'

const style = {
  wrapperStyle: {
    display: 'flex',
    alignItems: 'center',
  },
  buttonStyle: {
    flex: '0 0 auto',
  },
  spanStyle: {
    fontSize: '15px',
    fontWeight: '700',
    flex: '0 0 auto',
    color: 'rgba(0,0,0,0.7)',
  },
}


class LikeToggleButton extends React.Component {
  isAlreadyLiked(loginState) {
    for (let i = 0; i < this.props.likes.length; i += 1) {
      if (this.props.likes[i].user === loginState.user.id) {
        return true
      }
    }
    return false
  }

  toggle(userId) {
    this.props.toggleLike(this.props.post, userId)
  }

  render() {
    const loginState = JSON.parse(sessionStorage.getItem('loginState'))
    if (!loginState || !loginState.isLoggedOn) {
      return (
        <div style={{ ...this.props.style, ...style.wrapperStyle }}>
          <span style={style.spanStyle}>좋아요 {this.props.likes.length}</span>
          <IconButton style={style.buttonStyle} touch>
            <ActionFavoriteBorder color={grey100} />
          </IconButton>
        </div>
      )
    } else if (this.isAlreadyLiked(loginState)) {
      return (
        <div style={{ ...this.props.style, ...style.wrapperStyle }}>
          <span style={style.spanStyle}>좋아요 {this.props.likes.length}</span>
          <IconButton style={style.buttonStyle} touch onTouchTap={() => this.toggle(loginState.user.id)}>
            <ActionFavorite color={red500} />
          </IconButton>
        </div>
      )
    }
    return (
      <div style={{ ...this.props.style, ...style.wrapperStyle }}>
        <span style={style.spanStyle}>좋아요 {this.props.likes.length}</span>
        <IconButton style={style.buttonStyle} touch onTouchTap={() => this.toggle(loginState.user.id)}>
          <ActionFavoriteBorder color={grey400} />
        </IconButton>
      </div>
    )
  }
}

LikeToggleButton.propTypes = {
  likes: PropTypes.array,
  post: PropTypes.object,
  toggleLike: PropTypes.func,
  style: PropTypes.object,
}

const mapStateToProps = () => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleLike: (post, userId) => {
      dispatch(toggleLike(post, userId))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LikeToggleButton)
