import React from 'react'
import LoginForm from '../../molecules/LoginForm'
const formAlign={
  margin: '0',
  position: 'fixed',
  top: '50%',
  left: '50%',
  msTransform: 'translate(-50%, -50%)',
  transform: 'translate(-50%, -50%)'
}
const LoginPage = () => {
  return (
    <div style={formAlign}>
      <LoginForm/>
    </div>
  )
}

export default LoginPage
