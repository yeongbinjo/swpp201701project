import React, { PropTypes } from 'react'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import DatePicker from 'material-ui/DatePicker'
import { initialstate } from '../../../store/signUp/selectors'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton'
import { lightBlue600 } from 'material-ui/styles/colors'
import CircularProgress from 'material-ui/CircularProgress'
import { browserHistory } from 'react-router';
import Snackbar from 'material-ui/Snackbar';
import { connect } from 'react-redux'
import { postUserRequest, finishTransfer } from '../../../store/signUp/actions'
import { pushLogin } from '../../../store/login/actions'

const fontColor = lightBlue600

const styles={
  buttonStyles:{
    marginTop: '20%',
    marginLeft: '30%',
  },
  floatingLabelStyle:{
    fontStyle:'Roboto',
    color: fontColor,
    fontWeight:'1000',
  },
  RadioButtonGroup:{
    display:'inline-flex',
    marginTop:'10%',
    radioButton:{
      display:'inline-flex',
      marginRight:'20%',
      label:{
        fontWeight:'500',
      },
    },
    h4:{
      marginRight:'20%',
      color: fontColor,
      fontWeight:'500',
    }
  }
}



class SignUpForm extends React.Component {
  render(){
    let username, lastname, firstname, password, password2, birthday
    let gender = 0
    let password2error=''

    const disablePastDays=(date)=>{
      let today= new Date()
      return date>today
    }
    const handleDateChange=(event, date)=>{
      birthday = JSON.stringify(date).substring(1,11)
    }
    const handleGenderChange=(event, genderInput)=>{
      gender=genderInput
    }
    const handleCreateClick=() => {
      if(password.input.value !== password2.input.value){
        alert('passwords are different')
      }
      else if(gender ==0 ){
        alert('please select gender!')
      }
      else if(username.input.value.length<4){
        alert('ID is too short')
      }
      else if(lastname.input.value.length==0||firstname.input.value.length==0){
        alert('you should write name')
      }
      else if(password.input.value.length<4){
        alert('too short password')
      }
      else if(birthday==undefined){
        if(birthday2_signup=='')alert('you should choose birthday')
      }
      else{
          this.props.onPostUser(username.input.value, lastname.input.value, firstname.input.value, password.input.value, gender, birthday);
        }
      }

    const onFinish=()=>{
      this.props.finishTransfer()
      this.props.onLogin(username.input.value,password.input.value)
      this.context.router.push('/wall/'+this.props.statefunction.username)
    }

    const ButtonOrProgress = () =>{
      if(this.props.statefunction.signUpState==0){
        return(
          <div>
            <RaisedButton
              style={styles.buttonStyles}
              id="createbutton"
              label="Create"
              primary={true}
              onTouchTap={handleCreateClick}
              />
          </div>
        )
      }
      else {
        return(
          <CircularProgress />
        )
      }
    }

    return (
      <div>
        <TextField id="username_signup"
          floatingLabelText="ID"
          floatingLabelStyle={styles.floatingLabelStyle}
          ref={node => {username = node;}}
          />
        <TextField id="lastname_signup"
          floatingLabelText="Last Name"
          floatingLabelStyle={styles.floatingLabelStyle}
          ref={node => {lastname = node;}}
          />
        <TextField id="firstname_signup"
          floatingLabelText="First Name"
          floatingLabelStyle={styles.floatingLabelStyle}
          ref={node => {firstname = node;}}
          />
        <TextField id="password_signup"
          type="password"
          floatingLabelText="Password"
          floatingLabelStyle={styles.floatingLabelStyle}
          ref={node => {password = node;}}
          />
        <TextField id="password2_signup"
          type="password"
          floatingLabelText="Password Check"
          floatingLabelStyle={styles.floatingLabelStyle}
          errorText={password2error}
          ref={node => {password2 = node;}}
          />
        <div style={styles.RadioButtonGroup}>
          <h4 style={styles.RadioButtonGroup.h4}>Gender</h4>
          <RadioButtonGroup name="GENDER" onChange={handleGenderChange}>
            <RadioButton
              id="gender_male_signup"
              value="1"
              label="MALE"
              labelStyle={styles.RadioButtonGroup.radioButton.label}
              style={styles.RadioButtonGroup.radioButton}
            />
            <RadioButton
              id="gender_female_signup"
              value="2"
              label="FEMALE"
              labelStyle={styles.RadioButtonGroup.radioButton.label}
              style={styles.RadioButtonGroup.radioButton}
            />
          </RadioButtonGroup>
        </div>
        <DatePicker
          id="birthday_signup"
          floatingLabelText="Birthday"
          floatingLabelStyle={styles.floatingLabelStyle}
          shouldDisableDate={disablePastDays}
          onChange={handleDateChange}
          />
        <ButtonOrProgress/>
        <Snackbar
          id='goToWall'
          open={this.props.statefunction.signUpState==2}
          message={"Success!"}
          action="go to wall"
          autoHideDuration={1000000}
          onActionTouchTap={onFinish}
        />
      </div>
    );
  }
  static contextTypes = {
  router: PropTypes.object,
};

};


SignUpForm.propTypes = {
    reverse: PropTypes.bool,
    children: PropTypes.node,
}

const mapStateToProps = (state) => {
    return {
    statefunction : state.signUp
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    onPostUser: (username, lastname, firstname, password, gender, birthday) => {
      dispatch(postUserRequest(username, lastname, firstname, password, gender, birthday))
    },
    finishTransfer:()=>{
      dispatch(finishTransfer())
    },
    onLogin:(id, pw)=>{
      dispatch(pushLogin(id,pw))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm)
