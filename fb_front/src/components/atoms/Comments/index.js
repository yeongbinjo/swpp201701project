import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { List, ListItem } from 'material-ui/List'
import ActionSchedule from 'material-ui/svg-icons/action/schedule'


const style = {
  commentsStyle: {
    margin: '0',
    boxSizing: 'border-box',
  },
  listItemStyle: {
    textAlign: 'start',
    position: 'relative',
  },
  p: {
    textAlign: 'center',
    padding: '16px',
    margin: '0',
  },
  innerDivStyle: {
    padding: '4px 16px',
  },
  time: {
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
    fontSize: '10px',
    color: 'rgba(0,0,0,0.4)',
    top: '0',
    right: '16px',
  },
  icon: {
    width: '10px',
    height: '10px',
    marginRight: '2px',
  },
  commentContent: {
    margin: 0,
  },
}

const Comment = ({ text, date, author }) => {
  return (
    <ListItem
      style={style.listItemStyle}
      innerDivStyle={style.innerDivStyle}
      primaryText={`${author.last_name}${author.first_name}`}
      secondaryText={
        <div>
          <span style={style.time}><ActionSchedule style={style.icon} /> {moment(date).locale('ko').fromNow()}</span>
          <p style={style.commentContent}>{text}</p>
        </div>
      }
    />
  )
}

const Comments = ({ comments }) => {
  function getComments(comments) {
    if (comments.length > 0) {
      return (
        <List>
          {comments.map((comment, index) =>
            <Comment
              key={index}
              id={`comment_${index}`}
              text={comment.content}
              date={comment.created}
              author={comment.author}
            />
          )}
        </List>
      )
    } else {
      return (
        <p style={style.p}>아직 댓글이 없습니다. 가장 먼저 댓글을 달아주세요!</p>
      )
    }
  }

  return (
    <div style={style.commentsStyle}>
      {getComments(comments)}
    </div>
  )
}

Comments.propTypes = {
  comments: PropTypes.array,
}

Comment.propTypes = {
  text: PropTypes.string,
  date: PropTypes.string,
  author: PropTypes.object,
}

export default Comments
