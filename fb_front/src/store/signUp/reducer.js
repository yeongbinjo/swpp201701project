import { initialstate } from './selectors'

const signUpReducer = (state = initialstate, action) => {
  switch (action.type) {
    case 'POST_USER_REQUEST':
      return{
        ...state,
        signUpState:1
      }
    case 'USER_CREATED':
      return{
        ...state,
        signUpState:2,
        username: action.username
      }
    case 'CREATE_FAIL':
      return{
        ...state,
        signUpState:0
      }
    case 'FINISH_TRANSFER':
      return{
        ...state,
        signUpState:0
      }
    default:
      return state
  }
}

export default signUpReducer
