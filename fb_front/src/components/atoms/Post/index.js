import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { backendUrl } from 'config'
import { Card, CardActions, CardHeader, CardMedia, CardText } from 'material-ui/Card'
import { Link } from 'react-router'
import Divider from 'material-ui/Divider'
import TextField from 'material-ui/TextField'
import moment from 'moment'
import { getNewsFeeds } from '../../../store/newsFeed/actions'
import Comments from '../../atoms/Comments'
import { postComment } from '../../../store/comment/actions'
import LikeToggleButton from '../../organisms/LikeToggleButton/index'


const style = {
  cardStyle: {
    position: 'relative',
    borderRadius: '5px',
    border: '1px solid rgba(0,0,0,0.1)',
  },
  commentWrapperStyle: {
    padding: '0',
  },
  actionStyle: {
    padding: '0 16px',
  },
  inputStyle: {
    fontSize: '14px',
  },
  hintStyle: {
    fontSize: '14px',
  },
  dividerStyle: {
    margin: '0',
  },
  likeToggleButtonStyle: {
    position: 'absolute',
    top: '0',
    right: '0',
  },
  theme: {
    backgroundPosition: 'right top',
    backgroundSize: '931px 76px',
  },
  tagStyle: {
    margin: '4px',
    backgroundColor: '#cde69c',
    borderRadius: '2px',
    color: '#638421',
    border: '1px solid #a5d24a',
    padding: '2px',
    ':hover': {
      cursor: 'pointer',
    },
  },
}


class Post extends React.Component {
  static contextTypes = {
    router: PropTypes.object,
  }

  constructor() {
    super()

    this.onTagClick = this.onTagClick.bind(this)
    this.handleInput = this.handleInput.bind(this)
    this.handleKey = this.handleKey.bind(this)
  }

  onTagClick(tag) {
    this.context.router.push(`/tag/${tag}`)
    this.props.getNewsFeeds(tag)
  }

  handleInput(event, value) {
    this.setState({
      input: value,
    })
  }

  handleKey(event, value) {
    if (event.charCode === 13 && this.state.input !== '') {
      this.props.onComment(this.props.post.id, this.state.input)
      event.target.value = ''
      this.setState({
        input: '',
      })
    }
  }

  render() {
    const temp = backendUrl.replace('https', 'http')
    const media = this.props.post.image ? (
      <CardMedia>
        <img alt="이미지" src={this.props.post.image.includes(backendUrl) || this.props.post.image.includes(temp) ? this.props.post.image : `${backendUrl}${this.props.post.image}`} />
      </CardMedia>
    ) : (<div />)

    const authorFullname = `${this.props.post.author.last_name}${this.props.post.author.first_name}`
    const ownerFullname = `${this.props.post.owner.last_name}${this.props.post.owner.first_name}`
    const title = this.props.post.author.id === this.props.post.owner.id ? authorFullname : `${authorFullname} → ${ownerFullname}`

    const getCommentInput = () => {
      const loginState = JSON.parse(sessionStorage.getItem('loginState'))
      if (loginState && loginState.isLoggedOn) {
        return (
          <div>
            <Divider style={style.dividerStyle} />

            <CardActions style={style.actionStyle}>
              <TextField
                hintText="댓글 달기..."
                inputStyle={style.inputStyle}
                hintStyle={style.hintStyle}
                fullWidth
                onChange={this.handleInput}
                onKeyPress={this.handleKey}
              />
            </CardActions>
          </div>
        )
      }
      return (
        <div />
      )
    }

    const fromNow = moment(this.props.post.modified).locale('ko').fromNow()
    const subtitle = this.props.post.location !== '' ? `${fromNow} ― ${this.props.post.location}에서` : fromNow

    let avatarOrDefault = this.props.post.author.profile_image ? this.props.post.author.profile_image : '/img/avatar.png'
    if (avatarOrDefault.substring(0, 2) === '/m')avatarOrDefault = backendUrl + avatarOrDefault

    const themeNum = this.props.post.author.theme_number
    let themeImg
    for (const x of this.props.questState.questList) {
      if (x.id === themeNum) {
        themeImg = x.reward
      }
    }
    // 8064 * 712
    return (
      <Card style={style.cardStyle} zDepth={0}>
        <Link to={`/wall/${this.props.post.author.username}`}>
          <CardHeader
            title={title}
            subtitle={subtitle}
            avatar={avatarOrDefault}
            style={{ ...style.theme, backgroundImage: `url(${themeImg})` }}
          />
        </Link>
        <LikeToggleButton
          style={style.likeToggleButtonStyle}
          post={this.props.post} likes={this.props.post.likes}
        />

        {media}
        <CardText style={{ padding: '16px 16px 40px', position: 'relative' }}>
          {this.props.post.content}
          <div style={{ position: 'absolute', right: '16px', bottom: '16px' }}>
            {this.props.post.tags.map(tag =>
              <a key={tag.id} style={style.tagStyle} onClick={() => this.onTagClick(tag.name)}>&#35;{tag.name}</a>
            )}
          </div>
        </CardText>

        <Divider style={style.dividerStyle} />

        <CardText style={style.commentWrapperStyle}>
          <Comments comments={this.props.post.comments} />
        </CardText>

        {getCommentInput()}
      </Card>
    )
  }
}

Post.propTypes = {
  post: PropTypes.object,
  getNewsFeeds: PropTypes.func,
  onComment: PropTypes.func,
}

const mapStateToProps = (state) => {
  return {
    questState: state.quest,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getNewsFeeds: (tag) => {
      dispatch(getNewsFeeds(tag))
    },
    onComment: (index, text) => {
      dispatch(postComment(index, text))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)
