import { take, put, call, fork } from 'redux-saga/effects'
import api from 'services/api'
import * as actions from './actions'
import { backendUrl } from 'config'


const url = backendUrl + '/list/'

export function* postUser(username, last_name, first_name, password, gender, birthday){
  const response = yield call(fetch, url,{
    method:'POST',
    headers:{
      'Accept': 'application/json',
      'Content-Type'  : 'application/json'
    },
    body:JSON.stringify({
      username:username,
      password:password,
      first_name:first_name,
      last_name:last_name,
      gender:gender,
      birthday:birthday
    })
  })
  if(response.status==201){
    yield put({type:'USER_CREATED',  username: username})
  }
  else {
    alert('signup failed please try again')
    yield put({type:'CREATE_FAIL',})
  }
}

export function* watchPostUserRequest() {
    while (true) {
    const { username, lastname, firstname, password, gender,  birthday } = yield take('POST_USER_REQUEST')
    yield call(postUser, username, lastname, firstname, password, gender, birthday)
    }
}


export default function* () {
    yield fork(watchPostUserRequest)
}
