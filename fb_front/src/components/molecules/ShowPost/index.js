import React from 'react'
import PropTypes from 'prop-types'
import Post from '../../atoms/Post'

const ShowPost = ({ element, index }) => {
  return (
    <div>
      <Post id={`post_${index}`} post={element} />
    </div>
  )
}

ShowPost.propTypes = {
  element: PropTypes.object,
  index: PropTypes.number,
}

export default ShowPost
