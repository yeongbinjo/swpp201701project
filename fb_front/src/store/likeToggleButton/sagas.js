import { take, put, call, fork } from 'redux-saga/effects'
import api from 'services/api'
import { backendUrl } from 'config'
import { TOGGLE_LIKE } from './actions'
import { GET_NEWSFEEDS } from '../newsFeed/actions'

export function* toggleLike(post, userId) {
  const token = JSON.parse(sessionStorage.getItem('loginState')).token
  const response = yield call(api.get, `${backendUrl}/likes/?post=${post.id}&user=${userId}`, {
    headers: {
      Accept: 'application/json, application/xml, text/plain, text/html, *.*',
      Authorization: `Token ${token}`,
    },
  })
  if (response.length !== 0) {
    yield call(fetch, `${backendUrl}/likes/${response[0].id}/`, {
      method: 'DELETE',
      mode: 'cors',
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    })
  } else {
    yield call(api.post, `${backendUrl}/likes/`, { post: post.id, user: userId, like: '좋아요' }, {
      method: 'POST',
      mode: 'cors',
      headers: {
        Accept: 'application/json, application/xml, text/plain, text/html, *.*',
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    })
  }
  yield put({ type: GET_NEWSFEEDS })
  yield put({ type: 'WALL_PARTIAL_UPDATE', updateType: 0, id: post.owner.id })
}

export function* toggleLikeFlow() {
  while (true) {
    const { post, userId } = yield take(TOGGLE_LIKE)
    yield call(toggleLike, post, userId)
  }
}

export default function* () {
  yield fork(toggleLikeFlow)
}
