# please use after setting environmet variable,(BACKEND_URL = http://54.187.174.208:8000)

import time
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# browser = webdriver.Chrome()
browser = webdriver.Chrome('/usr/local/bin/chromedriver')
url = '{0}:{1}'.format(os.environ.get('HOST', 'localhost'), os.environ.get('PORT', 3000))

# home page
browser.get(url + '/')
time.sleep(1)

# Signup page - Sign up test

signUpButton = browser.find_element_by_id('signupButton_mini')
signUpButton.click()
time.sleep(1)

# fill in inputboxes
userID = 'test_id_0008'
password = 'test0008'

idBox = browser.find_element_by_id('username_signup')
idBox.send_keys(userID)
time.sleep(1)

lastNameBox = browser.find_element_by_id('lastname_signup')
lastNameBox.send_keys('Park')
time.sleep(1)

FirstNameBox = browser.find_element_by_id('firstname_signup')
FirstNameBox.send_keys('ChulSoo')
time.sleep(1)

PasswordBox = browser.find_element_by_id('password_signup')
PasswordBox.send_keys(password)
time.sleep(1)

PasswordBox2 = browser.find_element_by_id('password2_signup')
PasswordBox2.send_keys(password)
time.sleep(1)


GenderMaleButton = browser.find_element_by_id('gender_male_signup')
GenderMaleButton.click()
time.sleep(1)

GenderFemaleButton = browser.find_element_by_id('gender_female_signup')
GenderFemaleButton.click()
time.sleep(1)

BirthDayBox = browser.find_element_by_id('birthday_signup')
BirthDayBox.click()
time.sleep(1)

FirstDayBox = browser.find_element_by_xpath("/html/body/div/div/div[1]/div/div/div/div/div[2]/div[1]/div[3]/div/div/div[1]/button[1]")
FirstDayBox.click()
time.sleep(1)

DayCheckButton = browser.find_elements_by_xpath('/html/body/div/div/div[1]/div/div/div/div/div[2]/div[2]/button[2]/div')[0]
DayCheckButton.click()
time.sleep(1)

# Click createButtonprint(DayCheckButton)

CreateButton = browser.find_element_by_id('createbutton')
CreateButton.click()
time.sleep(3)

# Click go to mainpage
to_wall = browser.find_element_by_xpath("/html/body/main[@id='app']/div/section[1]/section[1]/div[2]/div/div[@id='goToWall']/div/div/button/div")
to_wall.click()
time.sleep(1)

# Use minilogin
logoutButton = browser.find_element_by_id('logoutButton_mini')
logoutButton.click()
time.sleep(2)

username_mini = browser.find_element_by_id('username_mini')
username_mini.send_keys(userID)
time.sleep(1)

password_mini =browser.find_element_by_id('password_mini')
password_mini.send_keys(password)
time.sleep(1)

loginButton = browser.find_element_by_id('loginButton_mini')
loginButton.click()
time.sleep(2)


# wall test
# Control textBox
time.sleep(3)
textBox = browser.find_element_by_id('textBox')
textBox.send_keys("hi this is test texts\n with enter!" + Keys.RETURN + "after one more enter")
time.sleep(3)

# Click  postButton
postButton = browser.find_element_by_id('postButton')
postButton.click()
time.sleep(3)

# Comment
commentInput = browser.find_element_by_id('commentInput_0')
commentInput.send_keys('hihihihihi\n')
time.sleep(2)

# like
like = browser.find_element_by_id('likeButton_like_0')
like.click()
time.sleep(1)

# cancel
like.click()
time.sleep(1)

# wow
wow = browser.find_element_by_id('likeButton_wow_0')
wow.click()
time.sleep(1)

# change
angry= browser.find_element_by_id('likeButton_angry_0')
angry.click()
time.sleep(1)

#logout
logoutButton = browser.find_element_by_id('logoutButton_mini')
logoutButton.click()
time.sleep(1)

browser.quit()
