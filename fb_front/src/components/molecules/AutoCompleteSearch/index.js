import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import AutoComplete from 'material-ui/AutoComplete'
import ActionSearch from 'material-ui/svg-icons/action/search'
import { connect } from 'react-redux'
import { getUserList } from '../../../store/userList/actions'

const SearchStyle = {
  marginBottom: '20px',
  marginLeft: '30px',
  width: '34%',
}

const style = {
  searchButtonStyle: {
    width: '36px',
    height: '36px',
  },
}

class AutoCompleteSearch extends React.Component {

  state={
    input: '',
    value: '',
  }

  componentDidMount() {
    this.props.onMount()
  }
  onInputChange = (searchText) => {
    this.setState({ input: searchText })
  }

  render() {
    const usernamelist = []
    for (const user of this.props.statefunction.userList) {
      usernamelist.push(user.last_name + user.first_name)
    }

    const temp = ''
    for (const user of this.props.statefunction.userList) {
      if (user.last_name + user.first_name === this.state.input) {
        this.state.value = user.username
      }
    }

    return (
      <div style={{ position: 'relative' }}>
        <AutoComplete
          id="search"
          floatingLabelText="친구를 찾아보세요!"
          filter={AutoComplete.caseInsensitiveFilter}
          dataSource={usernamelist}
          onUpdateInput={this.onInputChange}
          style={SearchStyle}
          floatingLabelStyle={{ color: 'white' }}
          hintStyle={{ color: 'white' }}
          inputStyle={{ color: 'white' }}
        />
        <Link to={`/wall/${this.state.value}`} style={{ position: 'absolute', right: 0, top: '24px' }}><ActionSearch style={style.searchButtonStyle} color="white" /></Link>

      </div>
    )
  }
}

AutoCompleteSearch.propTypes = {
  userList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    username: PropTypes.string,
    first_name: PropTypes.string,
    last_name: PropTypes.string,
  })),
}


const mapStateToProps = (state) => {
  return {
    statefunction: state.userList,

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onMount: () => {
      dispatch(getUserList())
    },
    // onPush: (userList) => {
    //   dispatch(FullnameList(userList))
    // },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AutoCompleteSearch)
