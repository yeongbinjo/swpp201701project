import React, { PropTypes } from 'react'
import Paper from 'material-ui/Paper'

const style = {
  leftWrapper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    margin: '5px',
  },

  rightWrapper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    margin: '5px',
  },

  bubble: {
    padding: '5px',
    flex: '0 1 auto',
  },
}

export class SpeechBubble extends React.Component {
  render() {
    const wrapperStyle = this.props.left ? style.leftWrapper : style.rightWrapper
    return (
      <div style={wrapperStyle}>
        <Paper style={style.bubble}>
          {this.props.message}
        </Paper>
      </div>
    )
  }
}

SpeechBubble.propTypes = {
  message: PropTypes.string,
  left: PropTypes.bool,
}
