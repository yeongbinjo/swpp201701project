// may be needed, to implement after post event
//

import { initialstate } from './selectors'

const postReducer = (state = initialstate, action) => {
  switch (action.type) {
    case "TEXT_TPYE":
      return{
        ...state,
        text:action.text,
      }
    case "IMAGE_CHANGE":
      return{
        ...state,
        image : action.image,
        imagePreviewURL : action.imagePreviewURL
      }
    case "PUSH_POST":
      return{
        ...state,
        postState: 1
      }
    case "POST_SUCCESS":
      if(state.postState==1){
        return{
          ...state,
          postState: 2
        }
      }
      else return initialstate
    case "AFTER_SUCCESS":
      return initialstate
    default:
      return state;
  }
}

export default postReducer
