export const initialstate = {
  isLoggedOn: false,
  user: {
    id:0,
    username:"",
    visited:[],
    quest_on:0,
    quest_done:[],
    theme_number:0
  },
  token:'no token',
}
export const getID = state => state.login.user.id;
export const getToken = state => state.login.token;
export const getLoginState = state=> state.login.isLoggedOn
export const getQuestDone = state=> state.login.user.quest_done
