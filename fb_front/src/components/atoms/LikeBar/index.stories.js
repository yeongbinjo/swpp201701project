import React from 'react'
import { storiesOf } from '@kadira/storybook'
import LikeBar from '.'

storiesOf('LikeBar', module)
  .add('default', () => (
    <LikeBar>Hello</LikeBar>
  ))
  .add('reverse', () => (
    <LikeBar reverse>Hello</LikeBar>
  ))
