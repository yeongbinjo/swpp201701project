import time
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

browser = webdriver.Chrome('/usr/local/bin/chromedriver')
url = '{0}:{1}'.format(os.environ.get('HOST', 'localhost'), os.environ.get('PORT', 3000))

# home page
browser.get(url + '/')
time.sleep(1)
# click timeline(it will directly go to login page)
browser.find_element_by_link_text('TimeLine').click()

signUpButton = browser.find_element_by_id('signup')
signUpButton.click()
time.sleep(1)

# Signup page - Sign up test
# fill in inputboxes
userID = 'test_id_0000'
password = 'test0000'

idBox = browser.find_element_by_id('username')
idBox.send_keys(userID)
time.sleep(1)

lastNameBox = browser.find_element_by_id('lastname')
lastNameBox.send_keys('Park')
time.sleep(1)

FirstNameBox = browser.find_element_by_id('firstname')
FirstNameBox.send_keys('ChulSoo')
time.sleep(1)

PasswordBox = browser.find_element_by_id('password')
PasswordBox.send_keys(password)
time.sleep(1)

GenderBox = browser.find_element_by_id('gender')
GenderBox.send_keys('1')
time.sleep(1)

BirthDayBox = browser.find_element_by_id('birthday')
BirthDayBox.send_keys('2000-01-01')
time.sleep(3)

# Click createButton
CreateButton = browser.find_element_by_id('createbutton')
CreateButton.click()
time.sleep(3)

# login
browser.find_element_by_link_text('TimeLine').click()

idBox = browser.find_element_by_id('ID')
idBox.send_keys(userID)
time.sleep(1)

passwordBox = browser.find_element_by_id('password')
passwordBox.send_keys(password)
time.sleep(1)

loginButton = browser.find_element_by_id('login')
loginButton.click()
time.sleep(2)


# timeline page - posting app test
browser.find_element_by_link_text('TimeLine').click()

# Control textBox
time.sleep(3)
textBox = browser.find_element_by_id('textBox')
textBox.send_keys("hi this is test texts\n with enter!" + Keys.RETURN + "after one more enter")
time.sleep(3)

# Click  postButton
postButton = browser.find_element_by_id('postButton')
postButton.click()
time.sleep(3)

browser.quit()
