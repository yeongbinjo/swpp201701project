import { take, put, call, fork } from 'redux-saga/effects'
import api from 'services/api'
import * as actions from './actions'
import { backendUrl } from 'config'


const url = backendUrl

export function* ownerSet(wallOwnerName) {
  try {
    // user info setting
    const ownerObject = yield call(api.get, `${url}/list/username/${wallOwnerName}/`)
    const ownerID = ownerObject.id
    // get post contents
    const response = yield call(api.get, `${url}/posts/?owner=${ownerID}`)
    for (const content of response) {
      const comments = yield call(api.get, `${url}/comments/?post=${content.id}`)
      content.comments = comments
      const likes = yield call(api.get, `${url}/likes/?post=${content.id}`)
      content.likes = likes
    }
    // in reverse order
    yield put({ type: 'SET_SUCCESS', wallOwner: ownerObject, wallPosts: response.reverse() })
  } catch (e) {
    console.log(e)
    yield put({ type: 'SET_FAILED' })
  }
}

export function* ownerSetFlow() {
  while (true) {
    const { wallOwnerName } = yield take('OWNER_SET')
    yield call(ownerSet, wallOwnerName)
  }
}

export function* partialPostUpdate(id) {
  const response = yield call(api.get, `${url}/posts/?owner=${id}`)
  for (const content of response) {
    const comments = yield call(api.get, `${url}/comments/?post=${content.id}`)
    const likes = yield call(api.get, `${url}/likes/?post=${content.id}`)
    content.comments = comments
    content.likes = likes
  }
  yield put({ type: 'PARTIAL_POST_UPDATE', wallPosts: response.reverse() })
}

export function* partialCommentUpdate(id) {
  const response = yield call(api.get, `${url}/comments/?post=${id}`)
  yield put({ type: 'PARTIAL_COMMENT_UPDATE', comments: response, postID: id })
}
export function* partialLikeUpdate(id) {
  const response = yield call(api.get, `${url}/likes/?post=${id}`)
  yield put({ type: 'PARTIAL_LIKE_UPDATE', likes: response, postID: id })
}

export function* partialUpdateFlow() {
  while (true) {
    const { updateType, id } = yield take('WALL_PARTIAL_UPDATE')
    if (updateType === 0) {
      yield call(partialPostUpdate, id)
    } else if (updateType === 1) {
      yield call(partialCommentUpdate, id)
    } else if (updateType === 2) {
      yield call(partialLikeUpdate, id)
    }
  }
}


export default function* () {
  yield fork(ownerSetFlow)
  yield fork(partialUpdateFlow)
}
