import React, { PropTypes } from 'react'
import styled, { css } from 'styled-components'
import { font, palette } from 'styled-theme'
import { ifProp } from 'styled-tools'
import { connect } from 'react-redux'
import { postComment } from '../../../store/comment/actions'

const commentInputStyle = {
  display: 'block',
  margin: '0',
  width: '100%',
  boxSizing: 'border-box',
  padding: '0.4444444444em',
  height: '2.24444444444em',
  border: '1px solid red',
  borderRadius: '2px',
}

const CommentInput = ({ statefunction, index, loginState, onComment, indexfortest }) => {
  let comment
  const onChange = (e) => {
    if (e.keyCode === 13) {
      comment.value = comment.value
      onComment(index, comment.value)
      comment.value = ''
    }
  }
  if (statefunction.commentState === 2) {
    document.getElementById('textBox').value = ''
  }
  if (loginState.isLoggedOn) {
    return (
      <div>
        <textarea id={`commentInput_${indexfortest}`} style={commentInputStyle} onKeyDown={onChange} ref={(node) => { comment = node }} placeholder={`${loginState.user.first_name}님! why don't you make some comments?`} />
      </div>
    )
  }

  return (
    <div>
       you should login if you want to make comments
     </div>
  )
}

const mapStateToProps = (state) => {
  return {
    statefunction: state.comment,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onComment: (index, text) => {
      dispatch(postComment(index, text))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentInput)

CommentInput.propTypes = {
  type: PropTypes.string,
  reverse: PropTypes.bool,
  height: PropTypes.number,
  invalid: PropTypes.bool,
}
