from django.contrib import admin
from webpush.models import Subscription

admin.site.register(Subscription)
