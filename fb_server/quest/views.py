import json

from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, AllowAny
from wall.permissions import IsOwnerOrReadOnly
from quest.models import Quest
from quest.serializers import QuestSerializer
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import get_user_model

User = get_user_model()


class QuestViewSet(viewsets.ModelViewSet):
    queryset = Quest.objects.all()
    serializer_class = QuestSerializer
    permission_classes = [AllowAny] # should be updated to admin-only
    filter_backends = [filters.DjangoFilterBackend,]
    filter_fields = ['name', ]


