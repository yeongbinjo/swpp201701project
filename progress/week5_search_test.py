# please use after setting environmet variable,(BACKEND_URL = http://54.187.174.208:8000)

import time
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# browser = webdriver.Chrome()
browser = webdriver.Chrome('/usr/local/bin/chromedriver')
url = '{0}:{1}'.format(os.environ.get('HOST', 'http://54.187.174.208'), os.environ.get('PORT', 3000))

# home page
browser.get(url + '/')
time.sleep(1)
print("Logging in as USER1...")
# fill in inputboxes
userID = 'USER1'
time.sleep(2)
password = 'user1111'
time.sleep(2)
username_mini = browser.find_element_by_id('username_mini')
username_mini.send_keys(userID)
time.sleep(1)

password_mini =browser.find_element_by_id('password_mini')
password_mini.send_keys(password)
time.sleep(1)

loginButton = browser.find_element_by_id('loginButton_mini')
loginButton.click()
time.sleep(2)
print("Successfully logged in!")
print("Now we wanna find TEST1...")
time.sleep(2)
textBox = browser.find_element_by_id('search')
textBox.send_keys("TEST1")
time.sleep(3)
searchButton = browser.find_element_by_id('submit')
searchButton.click()
time.sleep(3)


# # wall test
# # Control textBox
print("Leave a post for TEST1.")
time.sleep(3)
textBox = browser.find_element_by_id('textBox')
textBox.send_keys("HEY TEST1 ! IT'S GOOD TO SEE YOU !")
time.sleep(3)

# Click  postButton
postButton = browser.find_element_by_id('postButton')
postButton.click()
time.sleep(4)

#logout
logoutButton = browser.find_element_by_id('logoutButton_mini')
logoutButton.click()
time.sleep(1)

browser.quit()
print("Successfully finished!")