import likeReducer from './reducer'
import * as actions from './actions'
import { initialstate, getLikeState } from './selectors'
import totalinitialstate from '../totalinitialstate'

describe('like Reducer',() =>{

  it('has a default state',() =>{
    expect(likeReducer(undefined,{type:'unexpected'})).toEqual(
      initialstate
    )
  })
  it('try POST like',() =>{
    expect(likeReducer(undefined,{type:'POST_LIKE', index:1, text:'hi'})).toEqual({
      ...initialstate,
      likeState: 1
    })
  })
  it('try REVISE like',() =>{
    expect(likeReducer(undefined,{type:'REVISE_LIKE'})).toEqual({
      ...initialstate,
      likeState:1
    })
  })
  it('try DELETE like',() =>{
    expect(likeReducer(undefined,{type:'DELETE_LIKE'})).toEqual({
      ...initialstate,
      likeState:1
    })
  })
  it('like success',() =>{
    expect(likeReducer(undefined,{type:'LIKE_SUCCESS'})).toEqual({
      ...initialstate,
      likeState:2
    })
  })
  it('renew liking',() =>{
    expect(likeReducer(undefined,{type:'AFTER_LIKE_SUCCESS'})).toEqual({
      ...initialstate,
      likeState:0
    })
  })
  it('getLikeState',()=>{
    expect(getLikeState(totalinitialstate)).toEqual(0)
  })
})
