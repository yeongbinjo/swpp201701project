# coding: utf-8
import requests
import json
import os

backend_url = os.environ.get('BACKEND_URL', 'http://localhost:8000')

def test_user_can_view_post():
    data = json.loads(requests.get(backend_url).text)
    if data[0]['user'] == 2 and data[0]['context'] == 'Hello World!':
        print('사용자는 글을 조회할 수 있다.')

def test_user_can_post_post():
    # unittest 로 구현해놓음
    pass


if __name__ == '__main__':
    test_user_can_view_post()
    test_user_can_post_post()
