import { connect } from 'react-redux'
import { startChat, getUserList, follow, unfollow } from '../../store/userList/actions'
import { UserList } from '../../components/organisms/UserList'

const mapStateToProps = (state) => {
  return {
    userList: state.userList.userList,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onUserTap: (user) => {
      dispatch(startChat(user))
    },
    onMount: () => {
      dispatch(getUserList())
    },
    follow: (userId, followeeId) => {
      dispatch(follow(userId, followeeId))
    },
    unfollow: (userId, followeeId) => {
      dispatch(unfollow(userId, followeeId))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)
