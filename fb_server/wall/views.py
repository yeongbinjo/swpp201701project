import json

from django.db.models import Q
from django.shortcuts import get_object_or_404
from geoip2.errors import AddressNotFoundError
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny
from wall.permissions import IsOwnerOrReadOnly
from wall.models import Post, Comment, Like, Tag
from wall.serializers import PostSerializer, CommentSerializer, LikeSerializer, PostWriteSerializer
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import get_user_model
from django.contrib.gis.geoip2 import GeoIP2

g = GeoIP2()
User = get_user_model()


# http://stackoverflow.com/questions/43024529/django-drf-listcreateapiview-post-failing-with-depth-2
class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    filter_backends = [filters.DjangoFilterBackend,]
    filter_fields = ['owner', 'author']

    @staticmethod
    def _get_client_ip(request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    @staticmethod
    def _quest_is_done(user, visited):
        if not user.quest_on:
            return False
        for region in json.loads(user.quest_on.regions):
            if region not in visited:
                return False
        return True

    @staticmethod
    def _accomplish_quest(user):
        if not user.quest_on:
            return
        user.quest_finisher.add(user.quest_on)
        user.quest_on = None
        user.save()

    def _get_city(self, request):
        try:
            return g.city(self._get_client_ip(request))
        except AddressNotFoundError:
            return None

    @staticmethod
    def _get_country_name(city):
        if not city:
            return ''
        c = {
            'KR': 'Korea',
            'GB': 'United Kingdom',
            'FR': 'France',
            'JP': 'Japan',
            'US': 'United States',
            'CN': 'China',
        }
        return c[city['country_code']] if city['country_code'] in c else city['country_name']

    def _update_visited(self, request, country_name):
        if not country_name:
            return
        visited = json.loads(request.user.visited)
        if country_name not in visited:
            visited.append(country_name)
            request.user.visited = json.dumps(visited)
            request.user.save()
            if self._quest_is_done(request.user, visited):
                self._accomplish_quest(request.user)

    def create(self, request, *args, **kwargs):
        request.data['author'] = request.user.pk
        city = self._get_city(request)
        tags = list(set([Tag.objects.get_or_create(name=tag_name)[0] for tag_name in request.data['tags'].split(',') if tag_name]))
        del(request.data['tags'])
        serializer = PostWriteSerializer(data=request.data)
        if serializer.is_valid():
            obj = serializer.save()
            for tag in tags:
                obj.tags.add(tag)
            if city:
                country_name = self._get_country_name(city)
                obj.tags.add(Tag.objects.get_or_create(name=country_name)[0])
                obj.location = '%s, %s' % (city['city'], city['country_name'])
                obj.save()
                self._update_visited(request, country_name)
            headers = self.get_success_headers(serializer.data)
            new_c = PostSerializer(obj)
            return Response(data=new_c.data, status=status.HTTP_201_CREATED, headers=headers)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None, **kwargs):
        post = get_object_or_404(Post, pk=pk)
        tags = [Tag.objects.get_or_create(name=tag_name)[0].id for tag_name in request.data['tags']]
        serializer = PostWriteSerializer(post, data={'tags': tags}, partial=True)
        if serializer.is_valid():
            obj = serializer.save()
            headers = self.get_success_headers(serializer.data)
            new_c = PostSerializer(obj)
            return Response(data=new_c.data, status=status.HTTP_201_CREATED, headers=headers)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NewsFeedViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Post.objects.all()
    permission_classes = [AllowAny]
    filter_backends = [filters.DjangoFilterBackend]
    filter_fields = ['created', 'modified']

    def get_queryset(self):
        tag = self.request.query_params.get('tag', '')
        if not tag:
            return Post.objects.filter()
        else:
            return Post.objects.filter(tags__name__exact=tag)

    def list(self, request, **kwargs):
        if request.user.is_authenticated:
            followees = request.user.followees.all()
            queryset = self.get_queryset().filter(
                Q(author__in=followees) | Q(owner__in=followees) | Q(author=request.user) | Q(owner=request.user)
            ).order_by('-created')
        else:
            queryset = self.get_queryset().order_by('-created')
        serializer = PostSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, **kwargs):
        post = get_object_or_404(self.queryset, pk=pk)
        serializer = PostSerializer(post)
        return Response(serializer.data)


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [filters.DjangoFilterBackend]
    filter_fields = ['post']

    def perform_create(self, serializer):
        serializer.save(post_id=self.request.data['post'], author=self.request.user)


class LikeViewSet(viewsets.ModelViewSet):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = [IsAuthenticatedOrReadOnly,]
    filter_backends = [filters.DjangoFilterBackend,]
    filter_fields = ['post', 'user']

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

