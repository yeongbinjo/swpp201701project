import json

from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


class SubscriptionTestCase(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(username='tester', password='pwd12345678')

    def test_user_can_subscribe_webpush(self):
        """모든 사용자는 알림 등록을 할 수 있다"""
        token = Token.objects.get(user=self.user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.post('/subscriptions/', {'endpoint': 'http://test.com',
                                                   'p256dh': 'test',
                                                   'auth': 'test'}, format='json')
        self.assertEqual(response.status_code, 201)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(content['endpoint'], 'http://test.com')
        self.assertEqual(content['p256dh'], 'test')
        self.assertEqual(content['auth'], 'test')
