// may be needed, to implement after post event
//

import { initialstate } from './selectors'

const likeReducer = (state = initialstate, action) => {
  switch (action.type) {
    case "POST_LIKE":
      return{
        ...state,
        likeState: 1
      }
    case "REVISE_LIKE":
      return{
        ...state,
        likeState: 1
    }
    case "DELETE_LIKE":
      return{
        ...state,
        likeState : 1
    }
    case "LIKE_SUCCESS":
      return{
        ...state,
        likeState: 2
      }
    case "AFTER_LIKE_SUCCESS":
      return{
        ...state,
        likeState: 0
      }
    default:
      return state;
  }
}

export default likeReducer
