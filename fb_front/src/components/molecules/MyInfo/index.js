import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { updateLogin } from '../../../store/login/actions'
import PWChange from '../PWChange'
import Divider from 'material-ui/Divider'
import {List, ListItem} from 'material-ui/List'
import Paper from 'material-ui/Paper'
import ImageUpload from '../../atoms/ImageUpload'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {themeChangeRequest} from '../../../store/infoChange/actions'

import Accessibility from 'material-ui/svg-icons/social/mood';
import Cake from 'material-ui/svg-icons/social/cake';
import {amber700} from 'material-ui/styles/colors';
import {red800} from 'material-ui/styles/colors';

const styles = {
  paper: {
    marginTop:'10%',
    display: 'inline-flex',
    float: 'left',
    height:'600px',
    width:'100%',
  },
  image:{
    paddingRight:'0',
  },
  list:{
    marginTop:'40px',
    width:'100%',
  },
};

export class MyInfo extends React.Component{
  state ={
    value:this.props.loginState.user.theme_number
  }
  render(){
    const gender = this.props.loginState.user.gender==1? '남':'여';

    const handleChange = (event, index, value) => {
      this.setState({value})
      this.props.themeChange(value);
}

    let showTheme = []
    for(let x of this.props.questState.questList){
      if(this.props.loginState.user.quest_done.includes(x.id)){
        showTheme.push(x)
      }
    }

    return (
      <div style={styles.paper}>
        <ImageUpload user={this.props.loginState.user} style={styles.image} />
        <List style={styles.list}>
          <ListItem
            leftIcon={<Accessibility color={amber700} />}
            insetChildren={true}
            primaryText={this.props.loginState.user.last_name + this.props.loginState.user.first_name +" ( "+gender+" )"} />
          <ListItem
            leftIcon={<Cake color={red800} />}
            insetChildren={true}
            primaryText={"Birthday: "+this.props.loginState.user.birthday} />
          <PWChange user={this.props.loginState.user} isLoggedOn={this.props.loginState.isLoggedOn}/>
          <SelectField
            floatingLabelText="Theme"
            value={this.state.value}
            onChange={handleChange}
            autoWidth={true}
          >
            <MenuItem value={0} primaryText="기본" />
            {showTheme.map((elements,index)=>
              <MenuItem value={elements.id} primaryText={elements.name} key={index}/>
            )}
          </SelectField>
        </List>
      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    loginState: state.login,
    questState: state.quest
  }
}
const mapDispatchToProps= (dispatch)=>{
  return{
    loginSet: (token, account)=>{
      dispatch(updateLogin(token, account))
    },
    themeChange:(theme)=>{
      dispatch(themeChangeRequest(theme))
    },
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(MyInfo)
