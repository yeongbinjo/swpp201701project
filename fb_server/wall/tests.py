# coding: utf-8
import json

from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework.authtoken.models import Token

from wall.models import Post, Tag
from rest_framework.test import APIClient


class PostTestCase(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(username='tester', password='pwd12345678')
        Post.objects.create(owner=self.user, author=self.user, content="Hello World!")

    def test_user_can_view_post(self):
        """모든 사용자는 글을 조회할 수 있다"""
        client = APIClient()
        response = client.get('/posts/', format='json')
        content = response.content.decode('utf-8') if type(response.content) == bytes else response.content
        self.assertEqual(json.loads(content)[0]['author']['id'], 1)
        self.assertEqual(json.loads(content)[0]['content'], 'Hello World!')

    def test_user_can_post_post(self):
        """사용자는 글을 등록할 수 있다"""
        token = Token.objects.get(user=self.user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.post('/posts/', {'owner': self.user.pk, 'content': 'Hello SWPP!'}, format='json')
        self.assertEqual(response.status_code, 201)
        post = Post.objects.get(content='Hello SWPP!')
        self.assertEqual(post.owner.pk, 1)
        self.assertEqual(post.author.pk, 1)
        self.assertEqual(post.content, 'Hello SWPP!')


class NewsFeedEmptyTestCase(TestCase):
    def setUp(self):
        self.tester1 = get_user_model().objects.create_user(username='tester1', password='pwd12345678')

    def test_user_cannot_view_anything_when_no_post_exists(self):
        """관련된 글이 없으면 뉴스피드는 비어있어야 한다"""
        token = Token.objects.get(user=self.tester1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.get('/newsfeeds/', format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(len(content), 0)


class NewsFeedTestCase(TestCase):
    def setUp(self):
        self.tester1 = get_user_model().objects.create_user(username='tester1', password='pwd12345678')
        self.tester2 = get_user_model().objects.create_user(username='tester2', password='pwd12345678')
        self.tester3 = get_user_model().objects.create_user(username='tester3', password='pwd12345678')
        self.post1_1 = Post.objects.create(owner=self.tester1, author=self.tester1, content="Hello World! 1-1")
        self.post1_2 = Post.objects.create(owner=self.tester1, author=self.tester2, content="Hello World! 1-2")
        self.post2_1 = Post.objects.create(owner=self.tester2, author=self.tester1, content="Hello World! 2-1")
        self.post2_2 = Post.objects.create(owner=self.tester2, author=self.tester2, content="Hello World! 2-2")
        self.post3_3 = Post.objects.create(owner=self.tester3, author=self.tester3, content="Hello World! 3-3")
        self.tester1.followees.add(self.tester2)
        self.tester2.followees.add(self.tester1)
        self.tester2.followees.add(self.tester3)

    def test_user_can_view_newsfeed_when_he_or_she_has_followee(self):
        """사용자는 자신의 글과 자신이 팔로우 하고 있는 사람의 포스트를 조회할 수 있다"""
        token = Token.objects.get(user=self.tester1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.get('/newsfeeds/', format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(len(content), 4)
        self.assertEqual(content[0]['content'], 'Hello World! 2-2')
        self.assertEqual(content[1]['content'], 'Hello World! 2-1')
        self.assertEqual(content[2]['content'], 'Hello World! 1-2')
        self.assertEqual(content[3]['content'], 'Hello World! 1-1')

    def test_user_can_view_newsfeed_when_he_or_she_has_followees(self):
        """사용자는 자신의 글과 자신이 팔로우 하고 있는 사람들의 포스트를 조회할 수 있다"""
        token = Token.objects.get(user=self.tester2)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.get('/newsfeeds/', format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(len(content), 5)
        self.assertEqual(content[0]['content'], 'Hello World! 3-3')
        self.assertEqual(content[1]['content'], 'Hello World! 2-2')
        self.assertEqual(content[2]['content'], 'Hello World! 2-1')
        self.assertEqual(content[3]['content'], 'Hello World! 1-2')
        self.assertEqual(content[4]['content'], 'Hello World! 1-1')

    def test_user_can_view_newsfeed_when_he_or_she_has_no_followee(self):
        """사용자는 자신이 팔로우하고 있는 사람이 없으면 자신의 글만 볼 수 있다"""
        token = Token.objects.get(user=self.tester3)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.get('/newsfeeds/', format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(len(content), 1)
        self.assertEqual(content[0]['content'], 'Hello World! 3-3')

    def test_unauthorized_user_can_view_newsfeed(self):
        """비로그인 사용자는 임의의 뉴스피드를 볼 수 있다"""
        client = APIClient()
        response = client.get('/newsfeeds/', format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(len(content), 5)


class TagTestCase(TestCase):
    def setUp(self):
        self.tester = get_user_model().objects.create_user(username='tester', password='pwd12345678')
        self.post = Post.objects.create(owner=self.tester, author=self.tester, content="Hello World! 1-1")

    def test_can_tag_to_post(self):
        """사용자는 포스트에 태그를 달 수 있다"""
        client = APIClient()
        response = client.get('/posts/1/', format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(content['tags'], [])
        token = Token.objects.get(user=self.tester)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/posts/1/', {'tags': ['안녕하신가', '힘 세고', '강한', '태그']}, format='json')
        self.assertEqual(response.status_code, 201)
        post = Post.objects.get(pk=1)
        self.assertEqual(post.tags.count(), 4)

    def test_can_untag_to_post(self):
        """사용자는 포스트에 태그를 지울 수 있다"""
        self.post.tags.add(Tag.objects.create(name='안녕'))
        client = APIClient()
        response = client.get('/posts/1/', format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8') if type(response.content) == bytes else response.content)
        self.assertEqual(len(content['tags']), 1)
        token = Token.objects.get(user=self.tester)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.patch('/posts/1/', {'tags': []}, format='json')
        self.assertEqual(response.status_code, 201)
        post = Post.objects.get(pk=1)
        self.assertEqual(post.tags.count(), 0)
